{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "c4c0aeed",
   "metadata": {},
   "source": [
    "# C2-6 Le tri rapide\n",
    "\n",
    "Le tri rapide (an anglais _quicksort_) est un tri assez populaire, plutôt\n",
    "performant dans la plupart des cas. Il est moins complexe à implémenter en\n",
    "Python que le tri fusion (plus adapté à d'autres langages ou à d'autres types\n",
    "de données). Dans quelques cas particuliers, il est aussi mauvais que les\n",
    "tris simples vus en début d'année.\n",
    "\n",
    "Comme le tri fusion, le tri rapide est un tri récursif, qui utilise lui aussi\n",
    "la stratégie du « diviser pour régner ».\n",
    "\n",
    "C'est un tri assez facile à implémenter en place, qui n'est généralement pas stable.\n",
    "\n",
    "Son énoncé est simple : soit un élément quelconque d'une liste à trier, appelé le\n",
    "pivot, alors la liste triée est la concaténation de trois listes :\n",
    "\n",
    "  * la liste triée des éléments inférieurs ou égaux au pivot ;\n",
    "  * la liste contenant comme seul élément le pivot ;\n",
    "  * la liste triée des éléments strictement supérieurs au pivot.\n",
    "\n",
    "(Bien entendu, la comparaison large et la comparaison stricte peuvent être interverties.)\n",
    "\n",
    "On note alors que le problème du tri se ramène à nouveau au tri de deux listes plus\n",
    "petites.\n",
    "\n",
    "## Le choix du pivot\n",
    "\n",
    "Le pivot est un élément quelconque de la liste, ce qui amène à plusieurs stratégies :\n",
    "\n",
    "  * prendre un élément déterminé comme le premier ou le dernier élément de la liste ;\n",
    "  * prendre un élément au hasard dans la liste.\n",
    "\n",
    "Le premier choix a l'avantage d'être rapide à réaliser ; le second suppose la\n",
    "possibilité d'utiliser un module de tirage aléatoire d'un nombre, donc aussi\n",
    "l'appel à une fonction supplémentaire (le coût temporel d'un appel de fonction\n",
    "ne doit pas être négligé). Mais ce choix est catastrophique quand la liste\n",
    "initiale est _déjà_ triée (au lieu de couper la liste en deux parties égales,\n",
    "on va se contenter de recopier la liste presque entière d'un côté et de créer\n",
    "une liste vide d'un autre côté).\n",
    "\n",
    "Le second choix est statistiquement plus judicieux, mais ses inconvénients\n",
    "viennent d'être soulignés (coût temporel légèrement supérieur)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7a52fe13",
   "metadata": {},
   "source": [
    "## Version naïve du tri\n",
    "\n",
    "La popularité de ce tri tient en partie à l'existence de version très compactes\n",
    "(mais souvent de performance médiocres) de ce tri :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "83d1d101",
   "metadata": {},
   "outputs": [],
   "source": [
    "def quicksort(L: list) -> list:\n",
    "    if len(L) < 2: return L\n",
    "    pivot = L[-1]  # on prend le dernier élément comme pivot\n",
    "    return (\n",
    "               quicksort( [ e for e in L[:-1] if e <= pivot ] )\n",
    "             + [ pivot ]\n",
    "             + quicksort( [ e for e in L[:-1] if e > pivot ] )\n",
    "           )"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "70f2eff0",
   "metadata": {},
   "source": [
    "En réalité, ce code est assez médiocre car il multiplie la création de listes et\n",
    "des opérations peu performantes comme la concaténation.\n",
    "\n",
    "Test du code :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "104d22ba",
   "metadata": {
    "lines_to_next_cell": 2
   },
   "outputs": [],
   "source": [
    "quicksort([ 3, 7, 4, 1, 8, 2, 6, 5 ])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a6ad48b6",
   "metadata": {},
   "source": [
    "## Le tri rapide en place\n",
    "\n",
    "Contrairement au tri fusion, la programmation en place du tri rapide ne pose\n",
    "pas trop de difficultés.\n",
    "\n",
    "Les deux listes étant de longueur imprévisible, il faut simplement utiliser un\n",
    "pointeur pour savoir où s'arrête la première sous-liste et où commence la\n",
    "seconde :\n",
    "\n",
    "    Soit la liste : [ 3, 7, 4, 1, 8, 2, 6, 5 ]\n",
    "    On prend arbitrairement le dernier élément comme pivot (le 5).\n",
    "\n",
    "    On parcourt alors la liste de gauche à droite :\n",
    "      --> initialement le pointeur est à gauche [ | 3, 7, 4, 1, 8, 2, 6, (5) ]\n",
    "      --> l'élément 3 est plus petit que le pivot (5) ; on décale le pointeur à droite\n",
    "          le découpage devient :  [ 3 | 7, 4, 1, 8, 2, 6, (5) ]\n",
    "      --> l'élément 7 est plus grand que le pivot (5) ; on ne fait rien\n",
    "      --> l'élément 4 est plus petit que le pivot (5) ; on échange alors le 4\n",
    "          et le premier élément à droite du pointeur (pour faire de la place), puis\n",
    "          on décale le pointeur :  [ 3, 4 | 7, 1, 8, 2, 6, (5) ]\n",
    "      --> l'élément 1 est plus petit que le pivot (5) ; on procède de même ;\n",
    "          le découpage devient :  [ 3, 4, 1 | 7, 8, 2, 6, (5) ]\n",
    "      --> l'élément 8 est plus grand que le pivot (5), on ne fait rien ;\n",
    "      --> l'élément 2 est plus petit que le pivot (5), on procède comme plus haut ;\n",
    "          le découpage devient :  [ 3, 4, 1, 2 | 8, 7, 6, (5) ]\n",
    "      --> l'élément 6 est plus grand que le pivot (5), on ne fait rien.\n",
    "    On a obtenu les deux sous-listes souhaitées ; il ne reste plus qu'à lancer\n",
    "    l'appel récursif sur chacune des deux.\n",
    "      --> on peut cependant placer tout de suite le pivot au bon endroit grâce\n",
    "          à un dernier échange : [ 3, 4, 1, 2, (5), 7, 6, 8 ]\n",
    "\n",
    "On note dans l'exemple précédent que le pivot est positionné à sa place finale dès cette étape.\n",
    "\n",
    "![tri rapide](https://user-images.githubusercontent.com/36489953/42190383-0923306a-7e5d-11e8-86b3-1e9f7a79b782.gif)\n",
    "\n",
    "Voici un code partiel qui illustre le découpage de la liste en deux zones :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d649c4c3",
   "metadata": {},
   "outputs": [],
   "source": [
    "# la fonction ci-dessous prend en argument une liste L\n",
    "# elle analyse la section comprise entre index_start (inclus)\n",
    "# et index_end (exclus) ; elle échange les positions de\n",
    "# certains éléments pour avoir à gauche des éléments plus\n",
    "# petits qu'un pivot arbitraire, puis le pivot, puis des\n",
    "# éléments plus grands que le pivot ;\n",
    "# l'indice du pivot est retourné\n",
    "def split(L: list, index_start: int, index_end: int) -> int:\n",
    "    assert index_end > index_start # la zone doit comporter au moins un élément\n",
    "    pivot = L[index_end - 1] # le pivot est le dernier élément de la zone\n",
    "    pointeur = index_start\n",
    "    for k in range(index_start, index_end - 1):\n",
    "        # on choisit ici une inégalité stricte pour économiser quelques opérations\n",
    "        if L[k] < pivot:\n",
    "            L[k], L[pointeur] = L[pointeur], L[k]\n",
    "            pointeur += 1\n",
    "    # on positionne le pivot entre les deux zones\n",
    "    L[index_end - 1], L[pointeur] = L[pointeur], pivot\n",
    "    return pointeur"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c9247e45",
   "metadata": {},
   "source": [
    "Comme la fonction `split` travaille en place, elle n'a pas besoin de retourner une liste.\n",
    "\n",
    "On teste cette fonction sur l'exemple ci-dessus :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0204c615",
   "metadata": {},
   "outputs": [],
   "source": [
    "L = [ 3, 7, 4, 1, 8, 2, 6, 5 ]\n",
    "pointeur = split(L, 0, 8)\n",
    "print(pointeur, L)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f2e9fcd5",
   "metadata": {},
   "source": [
    "La double récursivité ne pose alors plus de problème particulier :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7b0ca68a",
   "metadata": {},
   "outputs": [],
   "source": [
    "# on applique quicksort à L sur [a, b[\n",
    "def quicksort(L: list, a: int, b: int) -> None:\n",
    "    if b - a > 1:\n",
    "        pointeur = split(L, a, b)\n",
    "        quicksort(L, a, pointeur)\n",
    "        quicksort(L, pointeur + 1, b)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d2b9e32e",
   "metadata": {},
   "source": [
    "Test du code en place :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7778d1ee",
   "metadata": {},
   "outputs": [],
   "source": [
    "L = [ 3, 7, 4, 1, 8, 2, 6, 5 ]\n",
    "quicksort(L, 0, len(L))\n",
    "print(L)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1e57459a",
   "metadata": {},
   "source": [
    "L'animation suivante montre trois cas distincts ; le pivot est ici choisi au centre de l'intervalle, ce qui mets les listes triées (ou retournées) dans une situation optimale :\n",
    "\n",
    "| liste aléatoire | liste presque triée | liste à l'envers |\n",
    "|:---:|:---:|:---:|\n",
    "| ![img](https://bs-uploads.toptal.io/blackfish-uploads/sorting_algorithms_page/content/illustration/animated_image_file/animated_image/27969/quick-sort-b49af55c5a190e17bf2e256890728a6c.gif) | ![img](https://bs-uploads.toptal.io/blackfish-uploads/sorting_algorithms_page/content/illustration/animated_image_file/animated_image/27977/quick-sort-ec1ec0e60947b6fc5ff837af519a513a.gif) | ![img](https://bs-uploads.toptal.io/blackfish-uploads/sorting_algorithms_page/content/illustration/animated_image_file/animated_image/27985/quick-sort-fddc1fdeba778690ee4a0edd50010c6b.gif) |"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "429a1ba2",
   "metadata": {},
   "source": [
    "## Pour aller plus loin\n",
    "\n",
    "La version usuelle du tri rapide est assez médiocre quand les éléments sont fréquemment répétés (le pivot aura une sélectivité amoindrie), d'où une amélioration possible consistant à collecter tous les pivots pour constituer la liste centrale, ce qui donne lieu à la concaténation suivante :\n",
    "\n",
    "  * la liste triée des éléments _strictement_ inférieurs au pivot ;\n",
    "  * la liste de tous les éléments égaux au pivot ;\n",
    "  * la liste triée des éléments strictement supérieurs au pivot.\n",
    "\n",
    "L'animation suivante montre la différence entre le tri rapide classique et la version à trois parties dans le cas d'éléments répétés :\n",
    "\n",
    "| version classique | quicksort-3 |\n",
    "|:---:|:---:|\n",
    "| ![img](https://bs-uploads.toptal.io/blackfish-uploads/sorting_algorithms_page/content/illustration/animated_image_file/animated_image/27993/quick-sort-ba7e9970bd4e54de1934ed52d6ac7bb1.gif) | ![img](https://bs-uploads.toptal.io/blackfish-uploads/sorting_algorithms_page/content/illustration/animated_image_file/animated_image/28025/quick-sort-3-way-84116e8407045029d1dc696418e9be2e.gif) |"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
