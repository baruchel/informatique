{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "e04e1907",
   "metadata": {},
   "source": [
    "# Parcours d'arbres (cours n° 1)\n",
    "\n",
    "Dans de nombreux problèmes, les données ont la forme d'un arbre. De telles\n",
    "structures sont assez simples à représenter informatiquement. Par exemple,\n",
    "l'arbre suivant :\n",
    "\n",
    "![arbre](https://upload.wikimedia.org/wikipedia/commons/3/33/Breadth-first-tree.svg)\n",
    "\n",
    "peut être représenté sous la forme d'un dictionnaire :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "13ec18ab",
   "metadata": {},
   "outputs": [],
   "source": [
    "tree = {\n",
    "    1: [2, 3, 4],        2: [5, 6],        3: [],\n",
    "    4:[7, 8],            5: [9, 10],       6: [],\n",
    "    7: [11, 12],         8: [],            9: [],\n",
    "   10: [],              11: [],           12: []\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "326ee8be",
   "metadata": {},
   "source": [
    "La question se pose alors de _parcourir_ l'arbre, c'est-à-dire d'itérer sur\n",
    "chaque nœud de l'arbre sans en oublier, dans un ordre pertinent.\n",
    "\n",
    "Plusieurs types de parcours existent :\n",
    "\n",
    "  * le parcours en largeur ;\n",
    "  * les parcours en profondeur (plusieurs variantes)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "456d9230",
   "metadata": {},
   "source": [
    "## Parcours en largeur et notion de file\n",
    "\n",
    "Le parcours en largeur se fait étage par étage en partant de la racine et en balayant\n",
    "tous les nœuds de même profondeur :\n",
    "\n",
    "![parcours1](https://upload.wikimedia.org/wikipedia/commons/4/46/Animated_BFS.gif)\n",
    "\n",
    "Reprenons l'exemple ci-dessus (dans la variable `tree`) ; il faut commencer par\n",
    "le nœud 1, puis parcourir ses trois fils 2, 3 et 4. Or, les nœuds 2 et 4 ont eux-mêmes\n",
    "des fils, qu'il faut mémoriser au passage sans interrompre le parcours en largeur.\n",
    "La structure de données la plus adéquates pour cela est la _file_ : elle implémente\n",
    "une structure FIFO (_first in, first out_), qui permet d'ajouter de nouveaux éléments\n",
    "tout en donnant la priorité aux éléments précédemment mémorisés."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4b1cbef1",
   "metadata": {},
   "outputs": [],
   "source": [
    "from collections import deque                                                             \n",
    "\n",
    "nodes = deque([1])  # on part de la racine\n",
    "\n",
    "while len(nodes) > 0:\n",
    "    n = nodes.pop()\n",
    "    print(\"Noeud\", n)\n",
    "    children = tree[n]\n",
    "    for c in children:\n",
    "        nodes.appendleft(c)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d87e6b32",
   "metadata": {},
   "source": [
    "On rappelle qu'il est préférable d'éviter les manipulations à gauche d'une liste en Python — les manipulations à droite en revanche sont en $O(1)$ ; les files sont précisément conçues pour autoriser les manipulations à gauche et à droite en $O(1)$."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "06235648",
   "metadata": {},
   "source": [
    "## Parcours en profondeur\n",
    "\n",
    "Le parcours en profondeur se fait par exploration complète des diverses branches :\n",
    "\n",
    "![parcours](https://upload.wikimedia.org/wikipedia/commons/7/7f/Depth-First-Search.gif)\n",
    "\n",
    "Le parcours en profondeur comporte plusieurs variantes et diverses implémentations intéressantes, reposant soit sur la notion de _pile_ soit sur la récursivité.\n",
    "\n",
    "### Parcours en profondeur avec pile\n",
    "\n",
    "On explore ici chaque nouvelle branche jusqu'au bout avant de remonter au dernier embranchement non encore exploré. Reprenons l'exemple initial\n",
    "\n",
    "![arbre](https://upload.wikimedia.org/wikipedia/commons/3/33/Breadth-first-tree.svg)\n",
    "\n",
    "Dans cet exemple, l'ordre retenu sera 1, 2, 5, 9, 10, 6, 3, 4, 7, 11, 12, 8.\n",
    "\n",
    "L'idée est d'empiler chaque nœud rencontré et de dépiler un nœud quand on l'explore ; contrairement à la file qui est une structure FIFO, la pile est une structure LIFO (_last in, first out_).\n",
    "\n",
    "En Python, les piles s'implémentent simplement à l'aide de listes puisque les deux opérations fondamentales, ajout et suppression d'un élément à droite, sont en $O(1)$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7ce22864",
   "metadata": {
    "lines_to_next_cell": 2
   },
   "outputs": [],
   "source": [
    "pile = [1]\n",
    "\n",
    "while len(pile) > 0:\n",
    "    n = pile.pop()\n",
    "    print(\"Noeud\", n)\n",
    "    children = tree[n]\n",
    "    # on empile les éléments du dernier vers le premier\n",
    "    for c in reversed(children):\n",
    "        pile.append(c)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5df631f5",
   "metadata": {},
   "source": [
    "### Parcours en profondeur récursif\n",
    "\n",
    "La notion de pile est fortement rattachée à celle de récursivité (car les appels récursifs\n",
    "sont empilés les uns sur les autres et traités du dernier empilé vers le premier).\n",
    "\n",
    "Il est donc particulièrement simple d'implémenter le parcours précédent à l'aide d'une fonction récursive :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "08cfd81a",
   "metadata": {},
   "outputs": [],
   "source": [
    "def parcours(noeud: int) -> None:\n",
    "    print(\"Noeud\", noeud)\n",
    "    for c in tree[noeud]:\n",
    "        parcours(c)\n",
    "parcours(1)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "67a22d30",
   "metadata": {},
   "source": [
    "(Dans le code précédent, il y a une condition d'arrêt dissimulée : quand un nœud n'a pas d'enfant, aucun appel récursif n'a lieu.)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8ebec091",
   "metadata": {},
   "source": [
    "### Parcours en profondeur d'un arbre binaire\n",
    "\n",
    "Un type particulier d'arbre, les arbres binaires, ont un intérêt particulier ; chaque nœud a au plus deux éléments :\n",
    "\n",
    "![arbre](https://upload.wikimedia.org/wikipedia/commons/d/d0/Arbre_binaire_ordonne.svg)\n",
    "\n",
    "On représentera cet arbre dans la variable `tree2` :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7704d4ce",
   "metadata": {
    "lines_to_next_cell": 2
   },
   "outputs": [],
   "source": [
    "tree2 = {\n",
    "    1: (2, 3),        2: (4, 5),        3: (None, 6),\n",
    "    4: (None, None),  5: (7, 8),        6: (9, None),\n",
    "    7: (None, None),  8: (None, None),  9: (None, None)\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f6a7c632",
   "metadata": {},
   "source": [
    "Trois types de parcours en profondeur sont alors envisagés :\n",
    "\n",
    "  * le parcours préfixe (semblable au précédent), qui traite un nœud _avant_ de traiter ses descendants ;\n",
    "  * le parcours postfixe (qui traite un nœud _après_ avoir traité ses descendants) ;\n",
    "  * le parcours infixe (qui traite un nœud après avoir traité la branche gauche et avant de traiter la branche droite)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fbf4f032",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Le parcours préfixe\n",
    "def parcours(n: int) -> None:\n",
    "    print(\"Noeud\", n)\n",
    "    gauche, droite = tree2[n]\n",
    "    if gauche != None: parcours(gauche)\n",
    "    if droite != None: parcours(droite)\n",
    "parcours(1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a4cf9df5",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Le parcours postfixe\n",
    "def parcours(n: int) -> None:\n",
    "    gauche, droite = tree2[n]\n",
    "    if gauche != None: parcours(gauche)\n",
    "    if droite != None: parcours(droite)\n",
    "    print(\"Noeud\", n)\n",
    "parcours(1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "60db4a61",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Le parcours infixe\n",
    "def parcours(n: int) -> None:\n",
    "    gauche, droite = tree2[n]\n",
    "    if gauche != None: parcours(gauche)\n",
    "    print(\"Noeud\", n)\n",
    "    if droite != None: parcours(droite)\n",
    "parcours(1)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
