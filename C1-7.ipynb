{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "97291d95",
   "metadata": {},
   "source": [
    "# Le tri comptage\n",
    "\n",
    "Le tri comptage est un tri spécifique, qui n'est adapté qu'à un certain type\n",
    "de données vérifiant certaines conditions. Ce n'est donc pas un tri général.\n",
    "\n",
    "Quand les conditions requises sont remplies, ce tri est très performant, car\n",
    "son _coût temporel_ est linéaire.\n",
    "\n",
    "Le tri comptage s'applique à des données, plutôt nombreuses (afin que la\n",
    "différence avec des tris plus généraux soit significative), mais dont les\n",
    "valeurs appartiennent à un ensemble fini sur un intervalle plutôt restreint.\n",
    "\n",
    "À nouveau des notes d'élèves (entières) fourniront un très bon exemple ;\n",
    "imaginons un concours où plusieurs dizaines de milliers d'élèves obtiennent\n",
    "chacun une note entre 0 et 1000.\n",
    "\n",
    "Le principe du tri repose sur la construction d'un histogramme (coût linéaire)\n",
    "puis sur la reconstruction de la liste en balayant l'histogramme de façon\n",
    "croissante (coût linéaire).\n",
    "\n",
    "Voici un exemple minimal sur une population de vingt élèves notés entre A et E :\n",
    "\n",
    "    Alfred   D     Berthe   A     Cécile   B     Didier   B     Emma     C\n",
    "    Félicien A     Gaston   C     Hector   E     Isidore  C     Julie    B\n",
    "    Katia    D     Lucien   B     Marie    C     Noémie   D     Octave   E\n",
    "    Patrice  C     Quentin  D     Roger    B     Sylvie   E     Thomas   A\n",
    "\n",
    "L'histogramme des notes est :\n",
    "\n",
    "    A : Berthe, Félicien, Thomas\n",
    "    B : Cécile, Didier, Julier, Lucien, Roger\n",
    "    C : Emma, Gaston, Isidore, Marie, Patrice\n",
    "    D : Alfred, Katia, Noémie, Quentin\n",
    "    E : Hector, Octave, Sylvie\n",
    "\n",
    "Il ne reste plus alors qu'à balayer les A, puis les B, etc.\n",
    "\n",
    "Ce tri est clairement _non comparatif_ et _stable_ ; ce n'est pas un tri en place\n",
    "(on sacrifie donc de l'espace au profit du temps)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "565e3c99",
   "metadata": {},
   "source": [
    "## Codage du tri\n",
    "\n",
    "Il est intéressant ici de disposer d'une clef de tri, c'est-à-dire d'une\n",
    "fonction qui à un objet donné associera sa place dans l'histogramme (par\n",
    "exemple une fonction qui à un couple prénom/note associera la note). On\n",
    "choisit ici de trier des tuples sur leur second élément :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5b6b9882",
   "metadata": {},
   "outputs": [],
   "source": [
    "def clef_note(student: tuple) -> int:\n",
    "    return student[1]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5ef8a48a",
   "metadata": {},
   "source": [
    "Pour des raisons de commodité dans la construction de l'histogramme, on exige\n",
    "que la clef retourne un (petit) entier (dans l'exemple ci-dessus, il serait de\n",
    "la responsabilité de la clef de retourner 0 pour un élève noté A, 1 pour un\n",
    "élève noté B, etc.).\n",
    "\n",
    "La fonction de tri utilisera elle-même la clef pour savoir comment placer\n",
    "chaque élément dans l'histogramme. On passe aussi à la fonction de tri la taille\n",
    "de l'histogramme."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5df3a2cd",
   "metadata": {},
   "outputs": [],
   "source": [
    "from typing import Callable, Any\n",
    "\n",
    "def tri_comptage(liste: list, key: Callable[[Any], int], nsize: int) -> list:\n",
    "    # création d'un histogramme vide\n",
    "    H = [ [] for _ in range(nsize) ]\n",
    "    # construction de l'histogramme\n",
    "    for e in liste:\n",
    "        k = key(e)\n",
    "        H[k].append(e)\n",
    "    # réécriture de la liste\n",
    "    out = []\n",
    "    for casier in H:\n",
    "        out.extend(casier)\n",
    "    return out"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b12e001d",
   "metadata": {},
   "source": [
    "Voici un jeu de données pour tester la fonction :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a181c2e2",
   "metadata": {
    "lines_to_next_cell": 2
   },
   "outputs": [],
   "source": [
    "notes = [\n",
    "  (\"Alfred\",   3), (\"Berthe\",   0), (\"Cécile\",   1), (\"Didier\",   1), (\"Emma\",     2), \n",
    "  (\"Félicien\", 0), (\"Gaston\",   2), (\"Hector\",   4), (\"Isidore\",  2), (\"Julie\",    1), \n",
    "  (\"Katia\",    3), (\"Lucien\",   1), (\"Marie\",    3), (\"Noémie\",   3), (\"Octave\",   4), \n",
    "  (\"Patrice\",  2), (\"Quentin\",  3), (\"Roger\",    2), (\"Sylvie\",   4), (\"Thomas\",   0) \n",
    "  ]\n",
    "print(tri_comptage(notes, clef_note, 5))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c66c8c92",
   "metadata": {},
   "source": [
    "## Un cas très spécifique $clef=valeur$\n",
    "\n",
    "Si l'on souhaite simplement trier des petits entiers positifs, il suffit de compter\n",
    "les effectifs puis de construire une version triée de la liste sans se référer aux\n",
    "objets originaux :\n",
    "\n",
    "Par exemple :\n",
    "\n",
    "    L = [3, 0, 0, 2, 1, 2, 1, 3, 3, 4, 3, 2, 1, 2, 0, 4, 3, 4]\n",
    "    Effectifs : 3 fois la valeur 0\n",
    "                3 fois la valeur 1\n",
    "                4 fois la valeur 2\n",
    "                5 fois la valeur 3\n",
    "                3 fois la valeur 4\n",
    "        -> [0, 0, 0, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 3, 4, 4, 4]\n",
    "\n",
    "Ce qui se code ainsi (sans clef de tri cette fois) :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2cbc557c",
   "metadata": {},
   "outputs": [],
   "source": [
    "def tri_comptage2(liste: list, nsize: int) -> list:\n",
    "    H = [ 0 ] * nsize\n",
    "    for e in liste:\n",
    "        H[e] = H[e] + 1\n",
    "    out = []\n",
    "    for i in range(nsize):\n",
    "        out.extend( [i] * H[i] )\n",
    "    return out"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8490faab",
   "metadata": {},
   "source": [
    "Test du code précédent :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9439638b",
   "metadata": {
    "lines_to_next_cell": 2
   },
   "outputs": [],
   "source": [
    "L = [3, 0, 0, 2, 1, 2, 1, 3, 3, 4, 3, 2, 1, 2, 0, 4, 3, 4]\n",
    "print(tri_comptage2(L, 5))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "83383acd",
   "metadata": {},
   "source": [
    "## L'élimination des doublons\n",
    "\n",
    "Si l'on restreint les effectifs possibles aux deux valeurs, soit 0, soit au moins 1 occurrence, on construit un _ensemble_ d'éléments non répétés.\n",
    "\n",
    "**Exercice** Il suffit de modifier une seule ligne de la fonction précédente pour y parvenir. Repérer et modifier cette ligne afin de tester cette variante :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9605e953",
   "metadata": {},
   "outputs": [],
   "source": [
    "# TODO: Modifier une ligne du code ci-dessous\n",
    "def construction_ensemble(liste: list, nsize: int) -> list:\n",
    "    H = [ 0 ] * nsize\n",
    "    for e in liste:\n",
    "        H[e] = H[e] + 1\n",
    "    out = []\n",
    "    for i in range(nsize):\n",
    "        out.extend( [i] * H[i] )\n",
    "    return out\n",
    "\n",
    "# Test du code\n",
    "L = [6, 0, 0, 2, 1, 8, 1, 3, 6, 4, 8, 2, 1, 2, 0, 4, 3, 4]\n",
    "print(construction_ensemble(L, 10))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c6300dae",
   "metadata": {},
   "source": [
    "Bien entendu, une version bien plus compacte est possible :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9bb81cd4",
   "metadata": {},
   "outputs": [],
   "source": [
    "def construction_ensemble2(liste: list, nsize: int) -> list:\n",
    "    H = [ False ] * nsize\n",
    "    for e in liste:\n",
    "        H[e] = True\n",
    "    return [ i for i in range(nsize) if H[i] ]\n",
    "\n",
    "# Test du code\n",
    "L = [6, 0, 0, 2, 1, 8, 1, 3, 6, 4, 8, 2, 1, 2, 0, 4, 3, 4]\n",
    "print(construction_ensemble2(L, 10))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4a7ba414",
   "metadata": {},
   "source": [
    "(Notons que si le domaine de la liste est plus complexe, le recours au dictionnaire sera typiquement préférable pour réaliser quelque chose de similaire.)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a87d83a6",
   "metadata": {},
   "source": [
    "## Pour aller plus loin\n",
    "\n",
    "Prouver que le coût de l'algorithme est _linéaire_. Prouver la _stabilité_ du tri comptage.\n",
    "\n",
    "Un algorithme de ce genre peut être rendu encore plus performant grâce à la parallélisation du décompte : on divise les données de départ en plusieurs sous-liste, le décompte des effectifs est calculé séparément par différents processus simultanés, puis on additionne les effectifs obtenus avant de terminer de façon classique."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
