{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "ef31916c",
   "metadata": {},
   "source": [
    "# Le tri par sélection\n",
    "\n",
    "Le tri par sélection est un tri médiocre mais proche de l'intuition humaine\n",
    "(si nous devons trier « à la main » une vingtaine d'objets, il arrive assez\n",
    "souvent que nous procédions de la sorte). L'idée est la suivante : prendre à\n",
    "chaque fois le plus petit élément non encore trié et l'ajouter à la suite de\n",
    "ce qui a déjà été trié."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a6047af8",
   "metadata": {},
   "source": [
    "## Version intuitive\n",
    "\n",
    "Exemple pour la liste de départ [11,23,7,4] :\n",
    "\n",
    "    Liste triée : []            Liste non triée : [11,23,7,4] (minimum: 4)\n",
    "                  [4]                             [11,23,7]   (minimum: 7)\n",
    "                  [4,7]                           [11,23]     (minimum: 11)\n",
    "                  [4,7,11]                        [23]        (minimum: 23)\n",
    "\n",
    "Écrire une fonction `tri_sélection` effectuant un tel tri sur une liste donné\n",
    "et retournant la liste triée. On s'autorisera ici à supprimer l'élément de la\n",
    "liste initiale à chaque fois. La fonction _retournera_ une (nouvelle) liste\n",
    "triée."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "03a77884",
   "metadata": {},
   "outputs": [],
   "source": [
    "def tri_selection1(liste: list) -> list:\n",
    "    out = []\n",
    "    while len(liste) > 0:\n",
    "        m, k = liste[0], 0 # valeur et position du minimum\n",
    "        for i in range(1, len(liste)):\n",
    "            if liste[i] < m:\n",
    "                m, k = liste[i], i\n",
    "        out.append(liste.pop(k))\n",
    "    return out\n",
    "\n",
    "print(tri_selection1([11, 23, 7, 4]))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "27aa80f7",
   "metadata": {
    "lines_to_next_cell": 2
   },
   "outputs": [],
   "source": [
    "# tester le code précédent\n",
    "from random import randrange\n",
    "L = [randrange(0, 100) for _ in range(20)]\n",
    "print(L)\n",
    "print(tri_selection1(L))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "596b8a41",
   "metadata": {},
   "source": [
    "## Version en place\n",
    "\n",
    "Deux problèmes se posent avec la version précédente :\n",
    "\n",
    "  * la liste de départ est détruite ;\n",
    "  * la suppression d'un élément à chaque tour a un coût significatif.\n",
    "\n",
    "On codera donc une variante _en place_ qui procède par échange : on échange\n",
    "le premier élément de la liste avec le plus petit élément de la liste, puis\n",
    "on échange le deuxième avec le plus petit restant, puis le troisième, etc.\n",
    "\n",
    "![tri sélection](https://user-images.githubusercontent.com/36489953/42171344-5554d9d2-7e19-11e8-8537-7811ebbbd1b6.gif)\n",
    "\n",
    "Exemple pour la liste de départ [11,23,7,4] :\n",
    "\n",
    "    Liste de travail : [11,23,7,4]\n",
    "                       [4,23,7,11] # après échange de 11 et de 4\n",
    "                       [4,7,23,11] # après échange de 23 et de 7\n",
    "                       [4,7,11,23] # après échange de 23 et de 11\n",
    "\n",
    "On conseille fortement ici l'utilisation d'une fonction auxiliaire `minindex_from_index`\n",
    "qui retourne l'indice de l'élément minimal à partir d'un certain indice ; on écrira\n",
    "donc en premier cette fonction :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f46df0a2",
   "metadata": {},
   "outputs": [],
   "source": [
    "def minindex_from_index(liste: list, ind: int) -> int:\n",
    "    m, k = liste[ind], ind\n",
    "    for i in range(ind+1, len(liste)):\n",
    "        if liste[i] < m:\n",
    "            m, k = liste[i], i\n",
    "    return k"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "250cbeab",
   "metadata": {},
   "source": [
    "On pourra alors écrire la version en place de l'algorithme. Observer la\n",
    "différence de signature de la fonction suivante (typique d'une fonction en\n",
    "place) :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "52649472",
   "metadata": {},
   "outputs": [],
   "source": [
    "def tri_selection2(liste: list) -> None:\n",
    "    for k in range(len(liste)):\n",
    "        i = minindex_from_index(liste, k)\n",
    "        liste[k], liste[i] = liste[i], liste[k]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7688c1e7",
   "metadata": {},
   "source": [
    "Cette version en place s'utilise différemment de la précédente (elle ne retourne rien) :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8823c7aa",
   "metadata": {},
   "outputs": [],
   "source": [
    "# tester le code précédent\n",
    "from random import randrange\n",
    "L = [randrange(0, 100) for _ in range(20)]\n",
    "print(L)\n",
    "tri_selection2(L)\n",
    "print(L)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ffc24f8a",
   "metadata": {},
   "source": [
    "L'animation suivante montre trois cas distincts qui ont le même coût temporel pour le tri sélection :\n",
    "\n",
    "| liste aléatoire | liste presque triée | liste à l'envers |\n",
    "|:---:|:---:|:---:|\n",
    "| ![img](https://bs-uploads.toptal.io/blackfish-uploads/sorting_algorithms_page/content/illustration/animated_image_file/animated_image/27809/selection-sort-f03ab6c4bee12d931a4899d1d48254fe.gif) | ![img](https://bs-uploads.toptal.io/blackfish-uploads/sorting_algorithms_page/content/illustration/animated_image_file/animated_image/27817/selection-sort-316f7c1ef1da5e8bcea0499e7102692e.gif) | ![img](https://bs-uploads.toptal.io/blackfish-uploads/sorting_algorithms_page/content/illustration/animated_image_file/animated_image/27825/selection-sort-5a466b70273c580220bdf7478dc43b05.gif) |"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7b1db5de",
   "metadata": {},
   "source": [
    "## Problème de la stabilité du tri\n",
    "\n",
    "On rappelle qu'un tri stable préserve l'ordre initial d'éléments égaux (par exemple,\n",
    "si l'on tri des élèves selon une note scolaire, deux élèves ayant reçu la même note\n",
    "devront conserver leur ordre relatif initial).\n",
    "\n",
    "Construire un contre-exemple simple constitué de quelques couples `(prénom, note)`\n",
    "prouvant la non stabilité du tri par sélection _en place_.\n",
    "\n",
    "Construire le contre-exemple minimal (en nombre d'éléments) et justifier."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
