# Cours d'informatique de MPSI

Réforme de 2021.

Les notebooks inclus dans ce dépôt sont accessibles depuis _Binder_ ; il vous suffit ce cliquer sur [ce lien](https://mybinder.org/v2/git/https%3A%2F%2Fcodeberg.org%2Fbaruchel%2Finformatique/HEAD) (l'initialisation de l'environnement peut être un peu long juste après une mise à jour du dépôt).
