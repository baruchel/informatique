{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "2c8013ca",
   "metadata": {},
   "source": [
    "# C2-3 Les algorithmes gloutons (partie 1)\n",
    "\n",
    "Une classe importante d'algorithmes simples à programmer et performants dans\n",
    "de nombreux problèmes est celle des [algorithmes gloutons](https://fr.wikipedia.org/wiki/Algorithme_glouton).\n",
    "\n",
    "On appelle _algorithme glouton_ (en anglais _greedy algorithm_), un algorithme itératif qui construit séquentiellement une solution à un problème donné en sélectionnant à chaque étape la solution partielle la plus intéressante sans tenir compte du reste de la solution.\n",
    "\n",
    "Par exemple, le problème du rendu de monnaie (qui sera étudié plus en détail) est fréquemment abordé de manière gloutonne : on sélectionne la plus grosse pièce qui « rentre » dans la somme à constituer, puis à nouveau la plus grosse pièce possible, et ainsi de suite.\n",
    "\n",
    "Dit encore autrement, un algorithme glouton s'efforce de construire une solution globale en maximisant à chaque étape un score simplement local."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "16abfb0b",
   "metadata": {},
   "source": [
    "## Illustration à partir d'un cas particulier du rendu de monnaie\n",
    "\n",
    "Le problème du rendu de monnaie est lié au système monétaire dans lequel il se pose. Le système monétaire est un ensemble fini de pièces en circulation. Par exemple, le système européen est le suivant (en centimes) : `[1, 2, 5, 10, 20, 50, 100, 200]`.\n",
    "\n",
    "Il s'avère que le système monétaire européen est particulièrement bien adapté à l'algorithme glouton (ce n'est pas le cas de tous les systèmes) : on prouve que l'algorithme glouton y minimise systématiquement le nombre de pièces utilisées pour constituer une somme.\n",
    "\n",
    "Pour constituer la somme de 37 centimes, on utilisera quatre pièces : `20, 10, 5, 2`.\n",
    "\n",
    "Voici le codage du rendu de monnaie dans le système monétaire européen :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "84e4ca60",
   "metadata": {},
   "outputs": [],
   "source": [
    "def rendu(somme: int) -> list:\n",
    "    S = [200, 100, 50, 20, 10, 5, 2, 1]\n",
    "    out = []\n",
    "    for p in S:\n",
    "        while p <= somme:\n",
    "            out.append(p)\n",
    "            somme = somme -p\n",
    "    return out\n",
    "\n",
    "rendu(37)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fbfddde9",
   "metadata": {},
   "source": [
    "On note que l'algorithme glouton suppose triée la liste des solutions partielles ; dans le cas du problème précédent, on peut estimer que la liste est d'emblée triée, mais dans de nombreux autres problèmes, la première étape consiste toujours à trier cette liste."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ff630fcf",
   "metadata": {},
   "source": [
    "## La décomposition en fractions égyptiennes\n",
    "\n",
    "On décompose un nombre rationnel $x$ dans l'intervalle $]0,1]$ en fractions égyptiennes, quand on le décompose en une somme finie de termes _distincts_ dont les numérateurs sont tous égaux à $1$. Par exemple, une décomposition possible de $3/4$ est $1/2+1/4$ ou encore $1/2+1/5+1/20$.\n",
    "\n",
    "Il existe plusieurs algorithmes pour construire cette décomposition ; l'un d'eux est l'algorithme glouton (découvert par Fibonacci). Il s'agit de retenir à chaque fois le plus grand rationnel de la forme $1/q$ possible (sans répétition).\n",
    "\n",
    "La preuve de cette construction (c'est-à-dire : la preuve que ce processus se termine) est assez aisée puisque le numérateur diminue à chaque étape (à prouver par vous-même !).\n",
    "\n",
    "Ici la liste est implicite : on parcourt la liste des inverses des entiers naturels non nuls : $1,1/2,1/3,1/4,1/5,1/6,\\dots$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6230fe91",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Décomposition de p/q en fractions égyptiennes\n",
    "def egyptian_fraction(p: int, q: int) -> list:\n",
    "    assert p <= q # on suppose p/q < 1\n",
    "    assert p > 0  # on suppose p positif\n",
    "    out = []\n",
    "    n = 1\n",
    "    while True:\n",
    "        if q <= n*p: # c'est-à-dire : 1/n <= p/q\n",
    "            out.append(n)\n",
    "            p, q = p*n - q, n*q  # nouvelle valeur de p/q (reste après soustraction)\n",
    "            if q%p == 0: break\n",
    "        n = n + 1\n",
    "    out.append( q // p ) # dernier terme, nécessairement de la forme 1/k\n",
    "    return out"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "69f8f177",
   "metadata": {},
   "source": [
    "On teste la fonction :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f2e9be2a",
   "metadata": {},
   "outputs": [],
   "source": [
    "egyptian_fraction(3, 7) # décomposition de 3/7"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0c7d9fbe",
   "metadata": {},
   "source": [
    "En réalité, le code précédent peut être amélioré : il n'est pas nécessaire de parcourir tous les entiers pour trouver le terme suivant, puisque l'on cherche à encadrer $p/q$ entre $1/(n+1)$ et $1/n$. Il suffit à chaque fois de prendre comme nouvelle valeur $n=\\lfloor[q/p\\rfloor]+1$ :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6e7d7e33",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Décomposition de p/q en fractions égyptiennes\n",
    "def egyptian_fraction(p: int, q: int) -> list:\n",
    "    assert p <= q # on suppose p/q < 1\n",
    "    assert p > 0  # on suppose p positif\n",
    "    out = []\n",
    "    n = 1\n",
    "    while q % p != 0:\n",
    "        n = q // p + 1\n",
    "        out.append(n)\n",
    "        p, q = p*n - q, n*q  # nouvelle valeur de p/q (reste après soustraction)\n",
    "    out.append( q // p ) # dernier terme, nécessairement de la forme 1/k\n",
    "    return out"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "64d0da9e",
   "metadata": {},
   "source": [
    "On teste la fonction :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0d566f71",
   "metadata": {},
   "outputs": [],
   "source": [
    "egyptian_fraction(3, 7) # décomposition de 3/7"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "44f75ffe",
   "metadata": {},
   "source": [
    "Bien que très optimisée, cette seconde version continue de suivre l'algorithme glouton dans son principe."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
