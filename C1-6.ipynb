{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "dd2abb7b",
   "metadata": {},
   "source": [
    "# Le tri à bulles\n",
    "\n",
    "> Le **tri à bulles** ou **tri par propagation** est un algorithme de tri. Il\n",
    "> consiste à comparer répétitivement les éléments consécutifs d'un tableau, et\n",
    "> à les permuter lorsqu'ils sont mal triés. Il doit son nom au fait qu'il\n",
    "> déplace rapidement les plus grands éléments en fin de tableau, comme des\n",
    "> bulles d'air qui remonteraient rapidement à la surface d'un liquide.\n",
    "> _(Source  : Wikipédia)_\n",
    "\n",
    "Le tri à bulle est donc un tri _comparatif_ ; il est par ailleurs _stable_.\n",
    "\n",
    "C'est un tri stable, bien connu des étudiants, relativement « populaire » car\n",
    "facile à mémoriser et à programmer mais notoirement médiocre dans la pratique."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6ad4ccdc",
   "metadata": {},
   "source": [
    "## Principe du tri\n",
    "\n",
    "L'idée est la suivante : on parcourt les éléments de la gauche vers la droite ;\n",
    "à chaque fois que l'on rencontre un élément plus petit que l'élément situé à sa\n",
    "gauche, on procède à un échange et l'on continue ensuite d'avancer vers la\n",
    "droite. Tant que ce parcours a donné lieu à au moins un échange, on recommence.\n",
    "L'algorithme prend fin quand la liste a été entièrement parcourue sans qu'aucun\n",
    "échange n'ait été effectué.\n",
    "\n",
    "![tri à bulles](https://user-images.githubusercontent.com/36489953/42171410-83532a64-7e19-11e8-95a1-b2dd3aaedc43.gif)\n",
    "\n",
    "Le tri à bulle repose donc sur deux boucles imbriquées. La boucle principale\n",
    "(extérieure) itère un nombre de fois non connu à l'avance ; la boucle\n",
    "intérieure parcourt l'ensemble de la liste de la gauche vers la droite et\n",
    "permute deux éléments adjacents s'ils sont mal ordonnés. Tant qu'au moins une\n",
    "permutation a eu lieu, la liste est considérée comme non triée et la boucle\n",
    "principale itère une fois de plus. Le nombre exact d'opérations dépend donc\n",
    "fortement de chaque liste initiale.\n",
    "\n",
    "Si la liste de départ est [11,23,7,4], le processus passera par les étapes suivantes\n",
    "    \n",
    "    [11, 23, 7, 4] (liste de départ)\n",
    "    [11, 7, 23, 4] (première boucle, échange du 23 et du 7)\n",
    "    [11, 7, 4, 23] (première boucle, échange du 23 et du 4)\n",
    "    [7, 11, 4, 23] (deuxième boucle, échange du 11 et du 7)\n",
    "    [7, 4, 11, 23] (deuxième boucle, échange du 11 et du 4)\n",
    "    [4, 7, 11, 23] (troisième boucle, échange du 7 et du 4)\n",
    "    [4, 7, 11, 23] (aucun échange au terme de la quatrième boucle)\n",
    "\n",
    "\n",
    "## Implémentation\n",
    "\n",
    "Voici une implémentation de ce tri. On note que le tri est _en place_ ; l'usage\n",
    "en Python est alors de ne pas retourner la structure modifiée (ici la liste).\n",
    "On en profite pour retourner des informations à valeurs statistiques qui ne\n",
    "font pas partie de l'algorithme : on compte le nombre de « passages »\n",
    "(c'est-à-dire le nombre de tours de la boucle extérieure)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e121a8b4",
   "metadata": {},
   "outputs": [],
   "source": [
    "def bubble_sort(l: list) -> int:\n",
    "    n = len(l)\n",
    "    stable = False\n",
    "    nsteps = 0\n",
    "    while not stable:\n",
    "        stable = True\n",
    "        for k in range(1, n):\n",
    "            if l[k] < l[k-1]:\n",
    "                l[k-1], l[k] = l[k], l[k-1]\n",
    "                stable = False\n",
    "        nsteps += 1\n",
    "    return nsteps"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4e8a965f",
   "metadata": {},
   "source": [
    "On note la présence d'une comparaison (dans la condition) : le tri est donc bien _comparatif_.\n",
    "\n",
    "Comme le tri est en place, il peut s'utiliser de la manière suivante :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "19e89adf",
   "metadata": {
    "lines_to_next_cell": 0
   },
   "outputs": [],
   "source": [
    "L = [7, 1, 3, 5, 2, 4, 6]\n",
    "\n",
    "# au choix\n",
    "# --------\n",
    "\n",
    "# liste L triée, nbr prend la valeur du nombre d'étapes\n",
    "nbr = bubble_sort(L)\n",
    "\n",
    "# liste L triée, le programmeur indique explicitement qu'il se désintéresse\n",
    "# de la valeur de retour de la fonction\n",
    "_ = bubble_sort(L)\n",
    "\n",
    "# fonction utilisée en tant qu'instruction\n",
    "bubble_sort(L)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "96981efe",
   "metadata": {},
   "source": []
  },
  {
   "cell_type": "markdown",
   "id": "ce86977a",
   "metadata": {},
   "source": [
    "## Observation des cas extrêmes\n",
    "\n",
    "On compte le nombre de passages dans des situations particulières remarquables :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fb76aeef",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Cas d'une liste déjà triée\n",
    "L = list(range(1000))                  # liste de longueur 1000\n",
    "bubble_sort(L)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d0684741",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Cas d'une liste à l'envers\n",
    "L = list(reversed(range(1000)))        # liste de longueur 1000\n",
    "bubble_sort(L)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b8c65486",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Présence d'un lièvre très éloigné de son point d'arrivée\n",
    "L = [ n%1000 for n in range(-1, 999) ] # liste de longueur 1000\n",
    "bubble_sort(L)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "920ee477",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Présence d'une tortue\n",
    "L = [ n%1000 for n in range(1, 1001) ] # liste de longueur 1000\n",
    "bubble_sort(L)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1f2e47f4",
   "metadata": {},
   "source": [
    "L'animation suivante montre trois cas distincts (cas moyen, meilleur cas, pire cas) :\n",
    "\n",
    "| liste aléatoire | liste presque triée | liste à l'envers |\n",
    "|:---:|:---:|:---:|\n",
    "| ![img](https://bs-uploads.toptal.io/blackfish-uploads/sorting_algorithms_page/content/illustration/animated_image_file/animated_image/27841/bubble-sort-ee720bc9e748ac03441804139ea1a5a7.gif) | ![img](https://bs-uploads.toptal.io/blackfish-uploads/sorting_algorithms_page/content/illustration/animated_image_file/animated_image/27849/bubble-sort-fe9aa235835a873d92d50c7fc9f21f92.gif) | ![img](https://bs-uploads.toptal.io/blackfish-uploads/sorting_algorithms_page/content/illustration/animated_image_file/animated_image/27857/bubble-sort-4ea522020296042eb7530040ff274848.gif) |"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fef93221",
   "metadata": {},
   "source": [
    "## Vérification empirique de la stabilité du tri\n",
    "\n",
    "Pour vérifier empiriquement que le tri est stable, on se propose de trier\n",
    "quelques élèves en fonction d'une note scolaire ; plusieurs élèves ayant\n",
    "la même note, on s'attend à ce qu'ils conservent leurs places respectives\n",
    "lors du tri.\n",
    "\n",
    "On modifie le code ci-dessus pour que des tuples `(nom, note)` soient\n",
    "triés en fonction du second élément :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1051aba5",
   "metadata": {
    "lines_to_next_cell": 2
   },
   "outputs": [],
   "source": [
    "def bubble_sort(l: list) -> int:\n",
    "    n = len(l)\n",
    "    ordered = False\n",
    "    nsteps = 0\n",
    "    while not ordered:\n",
    "        stable = True\n",
    "        for k in range(1, n):\n",
    "            if l[k][1] < l[k-1][1]: # modification du code\n",
    "                l[k-1], l[k] = l[k], l[k-1]\n",
    "                ordered = False\n",
    "        nsteps += 1\n",
    "    return nsteps\n",
    "\n",
    "L = [(\"Alice\", 15), (\"Bob\", 11), (\"Charles\", 13), (\"Denis\", 11),\n",
    "     (\"Emma\", 13), (\"Félicien\", 15), (\"Gaspard\", 12), (\"Hector\", 11)]\n",
    "\n",
    "bubble_sort(L)\n",
    "print(L)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3e672076",
   "metadata": {},
   "source": [
    "## Exercices\n",
    "\n",
    "**Exercice** Prouver la stabilité du tri.\n",
    "\n",
    "**Exercice** Prouver qu'après $k$ passages, les $k$ derniers éléments\n",
    "de la liste sont bien placés, et modifier le programme afin de réduire\n",
    "le nombre d'itérations de la boucle intérieure."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "78365dcf",
   "metadata": {},
   "source": [
    "## Pour aller plus loin\n",
    "\n",
    "Le _tri cocktail_ tente de corriger un défaut du tri à bulles en effectuant\n",
    "des passages dans des directions alternées (de la gauche vers la droite, puis\n",
    "de la droite vers la gauche) ; on pourra implémenter le tri cocktail. On\n",
    "pourra aussi améliorer le tri cocktail grâce à la même observation que\n",
    "précédemment : cette fois ce sont les deux bords qui sont triés avant les\n",
    "éléments centraux, et l'on peut donc à nouveau réduire l'intervalle à trier."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
