# Semestre 1

La plus grosse partie du 2.1 est soulignée dès le début de l'année
à titre de bonnes pratiques.

## Travail sur les listes (huit semaines)

  * thèmes 1, 2, 4

  Penser à utiliser des commentaires, des assert (éventuellement)
           signatures (annotations en Python)

  C1-1 : présentation, mise en route,
        rappels affectation, expression, instruction, boucle for
  T1-1 : Python comme calculatrice + module math
        affichage de triangle (chaînes de caractères)
  (maud)
  
  C1-2 : boucles while + tests (multiples, carrés, sommation dix premiers carrés, factorielle/boucle)
  T1-2 : révisions sans listes ni fonctions
        ex Euler n° 1
  (maud)

  C1-3 : fonctions + listes (len, append)
  T1-3 : mysum, mymin, mymax, average, stddev, horner
  (maud)
        
  C1-4 : dictionnaire, notion de coût,
        recherche séquentielle + recherche dichotomie
  T1-4 : construction d'un histogramme
        matplotlib : tracé de l'histogramme
        zéro d'une fonction
  (thomas)

  C1-5 : Cours autour d'un problème intéressant boucles imbriquées
        deux valeurs les plus proches dans une liste
  T1-5 : substring, eratosthene + euler
        chronométrage avec time de eratosthene 2n et n²
  (thomas)

  C1-6 : tri à bulles
  T1-6 : tri sélection
  (thomas)

  C1-7 : tri comptage
  T1-7 : tri insertion
  (thomas)
  
  Une semaine de réserve (corrigé, etc.)


  * fonctions mysum, mymin, mymax, average, stddev
              horner
  * construction d'un histogramme (sur listes/dictionnaires)

  * tri à bulle

  * recherche : mynindex (séquentielle),
                comparaison avec dictionnaire
                recherche dichotomique
  * boucles imbriquées : 

     * eratosthene 
     * sous-listes
     * terminer par tri sélection
     * (éventuellement tri comptage)
     * tri insertion

## Algorithmique (six semaines)

  C2-1 : récursivité factorielle, Hanoi
  T2-1 : exponentiation rapide + sous-ensembles
  (maud)

  C2-2 : nouvel exemple récursivité ???
  T2-2 : flocon de Koch avec turtle + deuxième problème ???
  (maud)
  
  C2-3 : algorithmes gloutons : rendu de monnaie + ???
  T2-3 : appliquer théorème de Zeckendorf + maximisation du nombre d'activités
  (thomas)

  C2-4 : non optimalité : problème du sac à dos, allocation mémoire
         https://www.geeksforgeeks.org/greedy-algorithms/
  T2-4 : ??? rendu de monnaie
  (thomas)

  C2-5 : cours tri fusion + complexité
  C2-5 : tri fusion
  (thomas)

  C2-6 : tri rapide : principe, notion de travail en place
  T2-6 : codage tri-rapide + tentative en place
         (vérification chronométrage)
  (thomas)

  * récursivité
  * algorithmes gloutons 
  * tri (fusion, rapide)


## Matrices et images (cinq semaines)

  C3-1 : numpy
           np.imshow
  T3-1 : contraste, seuil, éclaircir (une heure)
  (maud)

  C3-2 : rotation
  T3-2 : rotation (une heure)
  (maud)

  C3-3 : réduction, agrandissement
  T3-3 : réduction, agrandissement (une heure)
  (maud)

  C3-4 : convolution
  T3-4 : convolution (groupe 1)
  (maud)

  C3-5 : Numpy pur (thomas)
  T3-4 : convolution (groupe 2)

  * thèmes 3 et 7



# Semestre 2

## Méthode de programmation (rentrée février : trois semaines)

  C4-1 : variant, invariant
  T4-1 : PGCD + usage de commentaires, assert, etc.
         + deuxième exemple ???
         jeu de tests
         TODO: à revoir
  (maud)    TODO: s'arranger avec Maxime, TD de 2 * 1h

  C4-2 : complexité (dichotomie, PGCD)
  (thomas)
  T4-2 : TD josephus (groupe 1)

  C4-3 : complexité
  T4-2 : TD josephus (groupe 2)
  (thomas)

  * cours (terminaison, correction, complexité) trois semaines
  * TD algorithmique générale
    retour sur eratosthene

## Graphes (quatre semaines jusqu'aux vacances de Pâques)

  C5-1: généralités, définitions, propriétés, connexité (maud)
        + définition d'un arbre
  C5-2: graphe pondéré, examen de points liés à la pondération (maud)
  C5-3: parcours d'arbres (thomas) / largeur + profondeur
  C5-4: parcours de graphes (pile + récursivité) (thomas)

  T5-1 : fonctions graphes (matrices, listes d'arêtes, degré, nombre
            de sommets, etc.)    (maud)
  T5-2 : sudoku


  * cours graphes
  * TD graphes

## Graphes (suite : 4 semaines)

  C5-5: exemple manipulation graphe pondéré (euler ???)  (maud)
  C5-6: cyclicité + graphe planaire   (maud)
  C5-7: cours plus court chemin (Dijkstra)   (thomas)
  C5-8: files avec Fibonacci généralisé : (thomas)
         complexité spatiale en O(k)
         complexité temporelle en O(n) grâce aux files
         + problème des pompes à essence (geeks for geeks résolu avec
            tableau pour implémenter la queue)
            https://www.geeksforgeeks.org/find-a-tour-that-visits-all-stations/
        OU BIEN tout remplacer par algorithme de Tarjan
        codage de Huffman ?

  T5-3 : compter le nombre de régions connexes   (maud)
  T5-4 : arbre couvrant minimal (thomas)

  Deux TD débordent sur la partie suivante :
  (à cause de l'abstraction suivante ; ils se font en parallèle
  des cours sur la représentation des nombres)
  T5-5 : Dijkstra (maud)
  T5-6 : ??? (thomas) implémenter des arbres binaires de recherche ???
         codage de Huffman

## Représentation nombres (4 semaines)

  C6-1 : Généralités sur les bases, conversion
         Faire une remarque sur les entiers multi-précision
         (maud)
  C6-2 : Entiers relatifs, complément à deux
         (maud)
  C6-3 : Cours flottant
         (thomas)
  C6-4 : Cours flottant (suite) + limites
         module decimal, math.fsum
         (thomas)
  
