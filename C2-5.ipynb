{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "8f5ae29f",
   "metadata": {},
   "source": [
    "# C2-5 Le tri fusion\n",
    "\n",
    "Le tri fusion est l'un des tris les plus performants que l'on connaisse. Il est\n",
    "stable, comparatif. Il a la particularité d'avoir un excellent coût temporel\n",
    "dans tous les cas possibles ; c'est donc un excellent tri généraliste : sa\n",
    "complexité est _quasi-linéaire_ (cela sera formalisé ultérieurement mais on\n",
    "vérifiera dans cette séance que pour $n$ éléments, son coût temporal est\n",
    "proportionnel à $n \\log n$).\n",
    "\n",
    "Le tri fusion est en revanche plus complexe que d'autres à bien maîtriser, car\n",
    "il repose sur la récursivité.\n",
    "\n",
    "La récursivité est ici utilisée selon la stratégie du « diviser pour régner »,\n",
    "c'est-à-dire pour simplifier le problème. Plus précisément, on part du principe\n",
    "qu'il est plus simple de trier une petite liste qu'une grande (voire une liste\n",
    "de longueur 1 !), et qu'il est simple de procéder à la _fusion_ de deux listes\n",
    "triées.\n",
    "\n",
    "La fusion se comprend aisément si l'on imagine un paquet de cartes divisé en deux\n",
    "sous-paquets, chacun des deux étant trié. On peut alors reconstituer le paquet\n",
    "entier trié en choisissant à chaque fois quelle carte prendre entre les premières\n",
    "de chaque sous-paquet :\n",
    "\n",
    "    Paquet 1 :  [1, 4, 5, 8, 9]         Paquet 2 : [2, 3, 6, 7, 10]\n",
    "\n",
    "        --> on commence par prendre le 1 à gauche, puis le 2 et le 3 à droite, etc.\n",
    "\n",
    "Le tri fusion applique donc les règles suivantes :\n",
    "\n",
    "  * couper la liste non triée en deux parties égales ;\n",
    "  * si chaque partie obtenue est de longueur 1, les deux parties sont\n",
    "    considérées comme triées, sinon trier les deux parties par un\n",
    "    appel récursif ;\n",
    "  * fusionner les deux parties triées.\n",
    "\n",
    "![tri fusion](https://user-images.githubusercontent.com/36489953/42171944-ed5814c8-7e1a-11e8-9d30-10ae8047bb17.gif)\n",
    "\n",
    "On illustre d'abord la suite des appels récursifs pour la liste `[3, 0, 7, 5, 4, 1, 6, 2]` :\n",
    "\n",
    "                [3, 0, 7, 5, 4, 1, 6, 2]\n",
    "        [3, 0, 7, 5]                [4, 1, 6, 2]\n",
    "     [3, 0]      [7, 5]          [4, 1]      [6, 2]\n",
    "    [3]  [0]    [7]  [5]        [4]  [1]    [6]  [2]\n",
    "\n",
    "On illustre maintenant le désempilement des appels récursifs après les différentes fusions :\n",
    "\n",
    "                [3, 0, 7, 5, 4, 1, 6, 2]\n",
    "        [3, 0, 7, 5]                [4, 1, 6, 2]\n",
    "     [0, 3]      [5, 7]          [1, 4]      [2, 6]   (fusion des listes de longueur 1)\n",
    "\n",
    "                          puis\n",
    "\n",
    "                [3, 0, 7, 5, 4, 1, 6, 2]\n",
    "        [0, 3, 5, 7]                [1, 2, 4, 6]      (fusion des listes de longueur 2)\n",
    "                          puis\n",
    "\n",
    "                [0, 1, 2, 3, 4, 5, 6, 7]              (fusion des listes de longueur 4)\n",
    "\n",
    "\n",
    "## Codage de la fusion\n",
    "\n",
    "La fusion est sans doute l'étape la plus simple à programmer :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9ceeb514",
   "metadata": {},
   "outputs": [],
   "source": [
    "def fusion(A: list, B: list) -> list:\n",
    "    # on définit deux \"pointeurs\" indiquant où l'on en est dans A et dans B\n",
    "    i, j = 0, 0\n",
    "    out = []\n",
    "    # tant qu'il reste des comparaisons à faire entre les deux \"paquets\"\n",
    "    while i < len(A) and j < len(B):\n",
    "        if A[i] <= B[j]: # Attention : l'égalité ici préserve la stabilité\n",
    "            out.append(A[i])\n",
    "            i += 1\n",
    "        else:\n",
    "            out.append(B[j])\n",
    "            j += 1\n",
    "    # on recopie le \"bout\" restant dans l'une des deux listes\n",
    "    # (autant recopier les deux \"bouts\" puisque l'un des deux est vide !)\n",
    "    for k in range(i, len(A)): out.append(A[k])\n",
    "    for k in range(j, len(B)): out.append(B[k])\n",
    "    return out"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "beb04d25",
   "metadata": {},
   "source": [
    "On teste ainsi la fusion :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bde657a9",
   "metadata": {},
   "outputs": [],
   "source": [
    "fusion([1, 4, 5, 8, 9], [2, 3, 6, 7, 10])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0541d827",
   "metadata": {},
   "source": [
    "On remarque que la fusion est stable : si deux éléments identiques sont dans la même liste, ils conserveront leur ordre respectif ; on doit en revanche veiller à ce qu'un élément situé dans la liste de gauche soit prioritaire sur un élément identique situé dans la liste de droite lors de la comparaison (cf. commentaire dans le code donné ci-dessus).\n",
    "\n",
    "## Codage de la partie récursive\n",
    "\n",
    "La partie récursive n'est pas exagérément complexe ; on fera attention aux points suivants :\n",
    "\n",
    "  * la récursivité est double (la fonction s'appelle elle-même deux fois) ;\n",
    "  * la condition d'arrêt des appels récursifs est bien présente : l'appel récursif n'a lieu que sur des listes d'au moins deux éléments (puisque une liste vide ou une liste ne contenant qu'un seul élément est déjà triée)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ec4b43b5",
   "metadata": {},
   "outputs": [],
   "source": [
    "def tri_fusion(A: list) -> list:\n",
    "    n = len(A)          # longueur de la liste\n",
    "    m = n // 2          # longueur divisée par 2\n",
    "    u, v = A[:m], A[m:] # la liste est coupée en deux\n",
    "    if len(u) > 1: u = tri_fusion(u)\n",
    "    if len(v) > 1: v = tri_fusion(v)\n",
    "    return fusion(u, v)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8e0663d9",
   "metadata": {},
   "source": [
    "On peut finalement tester la fonction :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2904a8c6",
   "metadata": {},
   "outputs": [],
   "source": [
    "tri_fusion([1, 4, 5, 8, 9, 2, 3, 6, 7, 10])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f500f84f",
   "metadata": {},
   "source": [
    "La stabilité ici est garantie par la stabilité de la fusion (cf. supra).\n",
    "\n",
    "## Analyse de la complexité de la fonction\n",
    "\n",
    "On reprend le schéma déjà utilisé plus haut :\n",
    "\n",
    "                [3, 0, 7, 5, 4, 1, 6, 2]\n",
    "        [3, 0, 7, 5]                [4, 1, 6, 2]\n",
    "     [3, 0]      [7, 5]          [4, 1]      [6, 2]\n",
    "    [3]  [0]    [7]  [5]        [4]  [1]    [6]  [2]\n",
    "\n",
    "Le nombre d'étages dans les appels récursifs est simple à calculer. En admettant que la taille de la liste $n$ soit une puissance de 2, soit $n=2^k$, on note que $k$ étages sont empilés (soit $\\log_2 n$, puisque un nouvel étage est nécessaire tant que la taille précédente des listes peut encore être divisée par 2.\n",
    "\n",
    "En outre, à chaque étage, tous les éléments sont balayés lors des fusions, comme on le voit sur la suite du schéma :\n",
    "\n",
    "        […]\n",
    "     [0, 3]      [5, 7]          [1, 4]      [2, 6]   (fusion des listes de longueur 1)\n",
    "\n",
    "                          puis\n",
    "\n",
    "        […]\n",
    "        [0, 3, 5, 7]                [1, 2, 4, 6]      (fusion des listes de longueur 2)\n",
    "                          puis\n",
    "\n",
    "                [0, 1, 2, 3, 4, 5, 6, 7]              (fusion des listes de longueur 4)\n",
    "\n",
    "Il y aura donc $\\log_2 n$ étages avec pour chacun un temps de balayage proportionnel à $n$. La complexité du tri fusion est donc _quasi-linéaire_, soit proportionnelle à $n\\log n$."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7e124b5f",
   "metadata": {},
   "source": [
    "L'animation suivante montre trois cas distincts, qui ont le même coût temporal pour le tri fusion :\n",
    "\n",
    "| liste aléatoire | liste presque triée | liste à l'envers |\n",
    "|:---:|:---:|:---:|\n",
    "| ![img](https://bs-uploads.toptal.io/blackfish-uploads/sorting_algorithms_page/content/illustration/animated_image_file/animated_image/27905/merge-sort-8993d88bdfbaaceeccd892ffc089ed43.gif) | ![img](https://bs-uploads.toptal.io/blackfish-uploads/sorting_algorithms_page/content/illustration/animated_image_file/animated_image/27913/merge-sort-53e1128cb93e513c015268d2d83c2c30.gif) | ![img](https://bs-uploads.toptal.io/blackfish-uploads/sorting_algorithms_page/content/illustration/animated_image_file/animated_image/27921/merge-sort-0509469cacf182ca78aca960136f2dad.gif) |"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "47eb55e5",
   "metadata": {},
   "source": [
    "## Remarques complémentaires\n",
    "\n",
    "Attention, le codage précédent est en réalité suboptimal car il crée des listes supplémentaires (dans chacune des deux fonctions). Un codage en place est possible mais complexe et mal adapté à la structure de liste proposée par le langage Python."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
