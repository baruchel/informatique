{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "9be35080",
   "metadata": {},
   "source": [
    "# Utilisation de la dichotomie et des tableaux associatifs\n",
    "\n",
    "## Recherche du zéro d'une fonction par dichotomie\n",
    "\n",
    "Définir une fonction `solve`, travaillant par dichotomie, cherchant le zéro d'une fonction $f$, continue et monotone sur un intervalle donné, en supposant qu'en les bornes $a$ et $b$ de celui-ci, la fonction $f$ prend des valeurs de signes opposés.\n",
    "\n",
    "On utilisera une variable $\\epsilon$ (notée `epsilon` dans le code) pour désigner la précision souhaitée."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4c6032f9",
   "metadata": {},
   "outputs": [],
   "source": [
    "from typing import Callable\n",
    "\n",
    "def solve(f: Callable[[float], float], a: float, b: float, epsilon: float) -> float:\n",
    "    assert f(a) * f(b) < 0\n",
    "    while abs(a-b) > 2*epsilon:\n",
    "        c = (a+b)/2\n",
    "        if f(a)*f(c) > 0:\n",
    "            a = c\n",
    "        else:\n",
    "            b = c\n",
    "    return (a+b)/2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "632126c9",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Vérification\n",
    "from math import cos\n",
    "solve(cos, 1, 2, 1e-12) # on attend pi/2"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b0fa11c2",
   "metadata": {
    "lines_to_next_cell": 0
   },
   "source": [
    "## Construction d'un histogramme\n",
    "\n",
    "Définir une fonction `histogramme` prenant en argument une liste d'éléments quelconques et retournant un histogramme sous la forme d'un dictionnaire (on garantit que ces objets sont d'un type compatible avec la structure d'un dictionnaire).\n",
    "\n",
    "Voici quelques exemples de ce que doit retourner la fonction :\n",
    "\n",
    "    histogramme([1, 2, 3, 4, 5])            -->    { 1:1, 2:1, 3:1, 4:1, 5:1 }\n",
    "    histogramme([1, 1, 3, 3, 5])            -->    { 1:2, 3:2, 5:1 }\n",
    "    histogramme([\"alice\", \"bob\", \"bob\"])    -->    { \"alice\": 1, \"bob\": 2 }\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ba95af37",
   "metadata": {},
   "outputs": [],
   "source": [
    "def histogramme(liste: list) -> dict:\n",
    "    D = dict()\n",
    "    for e in liste:\n",
    "        if e in D:\n",
    "            D[e] = D[e] + 1\n",
    "        else:\n",
    "            D[e] = 1\n",
    "    return D"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d3368ea3",
   "metadata": {},
   "source": [
    "On testera la fonction sur la liste suivante :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7b6f5463",
   "metadata": {},
   "outputs": [],
   "source": [
    "L = [ 'alice', 'alice', 'charles', 'charles', 'bob', 'charles', 'denis',\n",
    "      'edouard', 'denis', 'edouard', 'bob', 'edouard', 'edouard', 'edouard',\n",
    "      'alice', 'alice', 'charles', 'edouard', 'alice', 'alice', 'bob',\n",
    "      'charles', 'alice' ]\n",
    "histogramme(L)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "67ef51ed",
   "metadata": {
    "lines_to_next_cell": 0
   },
   "source": [
    "Écrire ensuite une fonction `plot_histo` prenant en argument un histogramme (sous la forme d'un dictionnaire) et traçant l'histogramme sous la forme :\n",
    "\n",
    "\n",
    "    alice  #\n",
    "    bob    ##\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2628790e",
   "metadata": {},
   "outputs": [],
   "source": [
    "def plot_histo(H: dict) -> None:\n",
    "    for k in H:\n",
    "        print(k, \"#\" * H[k])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d18f39ce",
   "metadata": {},
   "source": [
    "## Reconstitution d'un itinéraire mélangé\n",
    "\n",
    "On considère une liste de trajets entre deux villes, telle que\n",
    "\n",
    "    [ (\"Brest\", \"Quimper\"), (\"Morlaix\", \"Guingamp\"), (\"Rennes\", \"Nantes\"), … ]\n",
    "\n",
    "Il est garanti qu'un itinéraire unique non cyclique permet de parcourir tous les trajets successivements pour aller d'une ville initiale à une ville finale.\n",
    "\n",
    "_Cette liste n'est pas ordonnée. Les trajets sont donc dans le désordre. Le problème\n",
    "consistera justement à reconstituer le bon ordre._\n",
    "\n",
    "Un bon algorithme consiste à :\n",
    "\n",
    "  * construire le dictionnaire qui associe les villes d'arrivée aux villes de départ ;\n",
    "  * construire le dictionnaire inverse du précédent ;\n",
    "  * parcourir la liste et détecter la ville qui n'est jamais une ville d'arrivée grâce au dictionnaire inverse ;\n",
    "  * partir de la ville de départ et construire l'itinéraire grâce au dictionnaire de base.\n",
    "\n",
    "_On constate que cet algorithme a un coût linéaire puisqu'il parcourt quatre fois de suite la liste de départ._\n",
    "\n",
    "Définir la fonction `itin` prenant en argument une liste de couples et retournant\n",
    "la liste décrivant l'itinéraire reconstitué."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7db6af8d",
   "metadata": {},
   "outputs": [],
   "source": [
    "def itin(l: list) -> list:\n",
    "    D1, D2 = dict(), dict()\n",
    "    for a, b in l:\n",
    "        D1[a], D2[b] = b, a\n",
    "    for x, _ in l:\n",
    "        if x not in D2:\n",
    "            break\n",
    "    r = [ x ]\n",
    "    while x in D1:\n",
    "        x = D1[x]\n",
    "        r.append(x)\n",
    "    return r"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
