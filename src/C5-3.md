---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.2'
      jupytext_version: 1.8.1
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

# Parcours d'arbres (cours n° 1)

Dans de nombreux problèmes, les données ont la forme d'un arbre. De telles
structures sont assez simples à représenter informatiquement. Par exemple,
l'arbre suivant :

![arbre](https://upload.wikimedia.org/wikipedia/commons/3/33/Breadth-first-tree.svg)

peut être représenté sous la forme d'un dictionnaire :

```python
tree = {
    1: [2, 3, 4],        2: [5, 6],        3: [],
    4:[7, 8],            5: [9, 10],       6: [],
    7: [11, 12],         8: [],            9: [],
   10: [],              11: [],           12: []
}
```

La question se pose alors de _parcourir_ l'arbre, c'est-à-dire d'itérer sur
chaque nœud de l'arbre sans en oublier, dans un ordre pertinent.

Plusieurs types de parcours existent :

  * le parcours en largeur ;
  * les parcours en profondeur (plusieurs variantes).


## Parcours en largeur et notion de file

Le parcours en largeur se fait étage par étage en partant de la racine et en balayant
tous les nœuds de même profondeur :

![parcours1](https://upload.wikimedia.org/wikipedia/commons/4/46/Animated_BFS.gif)

Reprenons l'exemple ci-dessus (dans la variable `tree`) ; il faut commencer par
le nœud 1, puis parcourir ses trois fils 2, 3 et 4. Or, les nœuds 2 et 4 ont eux-mêmes
des fils, qu'il faut mémoriser au passage sans interrompre le parcours en largeur.
La structure de données la plus adéquates pour cela est la _file_ : elle implémente
une structure FIFO (_first in, first out_), qui permet d'ajouter de nouveaux éléments
tout en donnant la priorité aux éléments précédemment mémorisés.

```python
from collections import deque                                                             

nodes = deque([1])  # on part de la racine

while len(nodes) > 0:
    n = nodes.pop()
    print("Noeud", n)
    children = tree[n]
    for c in children:
        nodes.appendleft(c)
```

On rappelle qu'il est préférable d'éviter les manipulations à gauche d'une liste en Python — les manipulations à droite en revanche sont en $O(1)$ ; les files sont précisément conçues pour autoriser les manipulations à gauche et à droite en $O(1)$.


## Parcours en profondeur

Le parcours en profondeur se fait par exploration complète des diverses branches :

![parcours](https://upload.wikimedia.org/wikipedia/commons/7/7f/Depth-First-Search.gif)

Le parcours en profondeur comporte plusieurs variantes et diverses implémentations intéressantes, reposant soit sur la notion de _pile_ soit sur la récursivité.

### Parcours en profondeur avec pile

On explore ici chaque nouvelle branche jusqu'au bout avant de remonter au dernier embranchement non encore exploré. Reprenons l'exemple initial

![arbre](https://upload.wikimedia.org/wikipedia/commons/3/33/Breadth-first-tree.svg)

Dans cet exemple, l'ordre retenu sera 1, 2, 5, 9, 10, 6, 3, 4, 7, 11, 12, 8.

L'idée est d'empiler chaque nœud rencontré et de dépiler un nœud quand on l'explore ; contrairement à la file qui est une structure FIFO, la pile est une structure LIFO (_last in, first out_).

En Python, les piles s'implémentent simplement à l'aide de listes puisque les deux opérations fondamentales, ajout et suppression d'un élément à droite, sont en $O(1)$.

```python
pile = [1]

while len(pile) > 0:
    n = pile.pop()
    print("Noeud", n)
    children = tree[n]
    # on empile les éléments du dernier vers le premier
    for c in reversed(children):
        pile.append(c)
```


### Parcours en profondeur récursif

La notion de pile est fortement rattachée à celle de récursivité (car les appels récursifs
sont empilés les uns sur les autres et traités du dernier empilé vers le premier).

Il est donc particulièrement simple d'implémenter le parcours précédent à l'aide d'une fonction récursive :

```python
def parcours(noeud: int) -> None:
    print("Noeud", noeud)
    for c in tree[noeud]:
        parcours(c)
parcours(1)
```

(Dans le code précédent, il y a une condition d'arrêt dissimulée : quand un nœud n'a pas d'enfant, aucun appel récursif n'a lieu.)


### Parcours en profondeur d'un arbre binaire

Un type particulier d'arbre, les arbres binaires, ont un intérêt particulier ; chaque nœud a au plus deux éléments :

![arbre](https://upload.wikimedia.org/wikipedia/commons/d/d0/Arbre_binaire_ordonne.svg)

On représentera cet arbre dans la variable `tree2` :

```python
tree2 = {
    1: (2, 3),        2: (4, 5),        3: (None, 6),
    4: (None, None),  5: (7, 8),        6: (9, None),
    7: (None, None),  8: (None, None),  9: (None, None)
}
```


Trois types de parcours en profondeur sont alors envisagés :

  * le parcours préfixe (semblable au précédent), qui traite un nœud _avant_ de traiter ses descendants ;
  * le parcours postfixe (qui traite un nœud _après_ avoir traité ses descendants) ;
  * le parcours infixe (qui traite un nœud après avoir traité la branche gauche et avant de traiter la branche droite).

```python
# Le parcours préfixe
def parcours(n: int) -> None:
    print("Noeud", n)
    gauche, droite = tree2[n]
    if gauche != None: parcours(gauche)
    if droite != None: parcours(droite)
parcours(1)
```

```python
# Le parcours postfixe
def parcours(n: int) -> None:
    gauche, droite = tree2[n]
    if gauche != None: parcours(gauche)
    if droite != None: parcours(droite)
    print("Noeud", n)
parcours(1)
```

```python
# Le parcours infixe
def parcours(n: int) -> None:
    gauche, droite = tree2[n]
    if gauche != None: parcours(gauche)
    print("Noeud", n)
    if droite != None: parcours(droite)
parcours(1)
```
