---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.2'
      jupytext_version: 1.8.1
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

# Parcours d'arbres (cours n° 2)

La résolution du Sudoku est assimilable à un parcours d'arbre en profondeur : on fait une hypothèse puis on explore toutes les hypothèses encore compatibles avec la précédente ; chaque hypothèse est donc assimilable à une branche que l'on explore.

Le procédé général du _backtracking_ consiste précisément à dégager une solution par exploration en profondeur, en remontant au dernier embranchement à chaque fois que l'on parvient à une impasse (et en effaçant les hypothèses accumulées à partir de ce dernier embranchement).

Voici une illustration de l'arbre d'hypothèses pour un jeu de grille plus ou moins similaire au Sudoku :

![tree](https://media.vlpt.us/images/kjh03160/post/64dbe07d-71bc-4cc4-8a7e-7c870a981bc8/image.png)


## Écriture d'un programme résolvant une grille de Sudoku

On définit pour commencer une fonction auxiliaire qui donne la liste des hypothèses compatibles pour une case $(x,y)$ donnée (cette fonction joue un rôle secondaire dans le cours sur les parcours d'arbres, et elle ne sera expliquée que sommairement) :

```python
def compatibles(grid: list, x:int, y: int) -> list:
    H = [ True ] * 10
    for z in range(9):
        H[ grid[y][z] ] = False     # incompatibilité / ligne
        H[ grid[z][x] ] = False     # incompatibilité / colonne
    bx, by = x//3, y//3
    for r in range(3*by, 3*by+3):
        for c in range(3*bx, 3*bx+3):
            H[ grid[r][c] ] = False # incompatibilité bloc
    return [ i for i in range(1, 10) if H[i] == True ]
```

La résolution du Sudoku devient alors triviale ; on propose ici une solution en place :

```python
def solve(grid: list) -> list:
    # On commence par recenser toutes les cases vides, et pour
    # chacune, on prend la listes des hypothèses possibles
    H = []
    for y in range(9):
        for x in range(9):
            if grid[y][x] == 0: H.append( ((x, y), compatibles(grid, x, y)) )

    # Si la grille est complète, l'exploration prend fin
    if len(H) == 0: return grid

    # on retient la case vide minimale pour le nombre d'hypothèses
    # (afin de maximiser les contraintes et accélérer la résolution)
    coords, hyps = min(H, key = lambda e: len(e[1]))
    x, y = coords

    # si le nombre d'hypothèses est nul, c'est une impasse, on doit remonter
    # dans l'arbre des hypothèses
    if len(hyps) == 0: return None

    # on explore chaque hypothèse possible pour la case (x,y)
    # par un parcours en profondeur récursif
    for h in hyps:
        # on "note" l'hypothèse dans la grille
        grid[y][x] = h
        # on explore la branche résultante
        result = solve(grid)
        # si un résultat est trouvé, on met fin à l'exploration
        # (sans explorer les dernières hypothèses)
        if result != None: return result

    # aucun résultat n'a été trouvé (ce qui signifie qu'une hypothèse
    # précédente est fausse)

    # on "efface" l'hypothèse dans la grille
    grid[y][x] = 0

    # on retourne une valeur d'échec
    return None
```

_(On notera le nombre de lignes de code assez restreint : même si un problème de programmation de ce genre peut sembler difficile au premier abord, il devient vraiment simple une fois assimilé à un parcours d'arbre.)_

Test de la fonction `solve` sur une grille de Sudoku « difficile » (mais résoluble par un être humain) :

```python
G = [[ 2, 0, 0,    6, 0, 0,    0, 0, 0 ],
     [ 5, 0, 0,    0, 1, 0,    8, 2, 0 ],
     [ 8, 0, 0,    9, 0, 0,    0, 4, 0 ],

     [ 0, 0, 0,    0, 0, 0,    0, 0, 0 ],
     [ 4, 0, 0,    0, 0, 5,    0, 7, 0 ],
     [ 0, 0, 9,    1, 0, 0,    0, 8, 0 ],

     [ 0, 0, 3,    7, 0, 0,    0, 1, 0 ],
     [ 0, 0, 0,    0, 0, 0,    6, 0, 2 ],
     [ 0, 0, 4,    0, 6, 0,    0, 0, 7 ]]

solve(G)
```

Test de la même fonction sur une grille non résoluble par un être humain :

```python
G2 = [[ 6, 0, 0,    0, 0, 8,    9, 4, 0 ],
      [ 9, 0, 0,    0, 0, 6,    1, 0, 0 ],
      [ 0, 7, 0,    0, 4, 0,    0, 0, 0 ],

      [ 2, 0, 0,    6, 1, 0,    0, 0, 0 ],
      [ 0, 0, 0,    0, 0, 0,    2, 0, 0 ],
      [ 0, 8, 9,    0, 0, 2,    0, 0, 0 ],

      [ 0, 0, 0,    0, 6, 0,    0, 0, 5 ],
      [ 0, 0, 0,    0, 0, 0,    0, 3, 0 ],
      [ 8, 0, 0,    0, 0, 1,    6, 0, 0 ]]

solve(G2)
```
