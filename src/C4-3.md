---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.2'
      jupytext_version: 1.8.1
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

# La complexité (cours n° 2)

## Exemples de cours

### Complexité de l'algorithme d'Euclide

La complexité sera calculée au tableau à partir de deux cas : $b \leqslant a/2$ ou $b>a/2$
avec $a$ et $b$ les deux entiers manipulés à chaque étape de l'algorithme.


### Complexité de l'exponentiation rapide

TODO


### Complexité de quelques tris

La complexité de divers tris sera étudiée au tableau :

  * le tri à bulles ;
  * le tri sélection ;
  * le tri comptage ;
  * le tri par insertion ;
  * le tri fusion ;
  * le tri rapide.


### Le problème de la force brute

Calculer la complexité de la résolution par force brute des deux problèmes suivants :

  * problème du voyageur de commerce :
  * problème de la satisfiabilité d'une formule du calcul propositionnel.


### Un algorithme original : la médiane rapide

Quelle est la complexité de la recherche traditionnelle de la médiane ?

On s'inspire de l'algorithme du tri rapide pour proposer l'algorithme suivant :

  * un pivot est choisi dans la liste ;
  * on partitionne la liste autour du pivot ;
  * on détermine dans quel segment la médiane doit se trouver ;
  * on effectue un unique appel récursif sur le segment détermine.

Exemple avec `[ 4, 5, 9, 0, 7, 8, 6, 2, 1, 3 ]`.
