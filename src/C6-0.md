# Introduction à la programmation dynamique

## Problème n° 14 du Projet Euler


The following iterative sequence is defined for the set of positive integers:

    n → n/2 (n is even)
    n → 3n + 1 (n is odd)

Using the rule above and starting with 13, we generate the following sequence:

    13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1

It can be seen that this sequence (starting at 13 and finishing at 1) contains 10
terms. Although it has not been proved yet (Collatz Problem), it is thought that all
starting numbers finish at 1.

Which starting number, under one million, produces the longest chain?

NOTE: Once the chain starts the terms are allowed to go above one million.


### Approche naïve

Définir une fonction auxiliaire calculant la période associée à un entier $n$ :

```python
def period(n):
    p = 1
    while n != 1:
        p = p + 1
        if n%2 == 0:
            n = n // 2
        else:
            n = 3*n+1
    return p
```

Recherche du point de départ associé à la plus longue période :

```python
def test1(n):
    mymax, start = 0, 0
    for i in range(1, n+1):
        p = period(i)
        if p > mymax:
            mymax, start = p, i
    return start
```

```python
%timeit test1(100000)
```

**Peut-on résoudre le problème n° 14 par l'approche naïve ?**

```python
# Attention : interrompre l'exécution après quelques secondes
%timeit test1(1000000)
```

### Programmation dynamique (première version)

```python
def test2(n):
    P, mymax, start = [ 0 ], 0, 0
    for i in range(1, n+1):
        j, p = i, 1
        while j != 1:
            if j < i:
                p = P[j] + p - 1
                break
            p = p + 1
            if j%2 == 0:
                j = j // 2
            else:
                j = 3*j+1
        P.append(p)
        if p > mymax:
            mymax, start = p, i
    return start
```

```python
%timeit test2(100000)
```

```python
%timeit test2(1000000)
```

### Programmation dynamique (seconde version)

```python
def test3_aux(n, P):
    if n < len(P) and P[n] > 0:
        return P[n]
    if n % 2 == 0:
        p = 1 + test3_aux(n//2, P)
    else:
        p = 1 + test3_aux(3*n+1, P)
    if n < len(P):
        P[n] = p
    return p
```

```python
def test3(n):
    P, mymax, start = [0] * (n+1), 0, 0
    P[1] = 1
    for i in range(2, n+1):
        p = test3_aux(i, P)
        if p > mymax:
            mymax, start = p, i
    return start
```

```python
%timeit test3(100000)
```

```python
%timeit test3(1000000)
```
