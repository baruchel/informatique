---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.2'
      jupytext_version: 1.8.1
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

# Utilisation de la dichotomie et des tableaux associatifs

## Recherche du zéro d'une fonction par dichotomie

Définir une fonction `solve`, travaillant par dichotomie, cherchant le zéro d'une fonction $f$, continue et monotone sur un intervalle donné, en supposant qu'en les bornes $a$ et $b$ de celui-ci, la fonction $f$ prend des valeurs de signes opposés.

On utilisera une variable $\epsilon$ (notée `epsilon` dans le code) pour désigner la précision souhaitée.

```python
from typing import Callable

def solve(f: Callable[[float], float], a: float, b: float, epsilon: float) -> float:
    assert f(a) * f(b) < 0
    while abs(a-b) > 2*epsilon:
        c = (a+b)/2
        if f(a)*f(c) > 0:
            a = c
        else:
            b = c
    return (a+b)/2
```

```python
# Vérification
from math import cos
solve(cos, 1, 2, 1e-12) # on attend pi/2
```

## Construction d'un histogramme

Définir une fonction `histogramme` prenant en argument une liste d'éléments quelconques et retournant un histogramme sous la forme d'un dictionnaire (on garantit que ces objets sont d'un type compatible avec la structure d'un dictionnaire).

Voici quelques exemples de ce que doit retourner la fonction :

    histogramme([1, 2, 3, 4, 5])            -->    { 1:1, 2:1, 3:1, 4:1, 5:1 }
    histogramme([1, 1, 3, 3, 5])            -->    { 1:2, 3:2, 5:1 }
    histogramme(["alice", "bob", "bob"])    -->    { "alice": 1, "bob": 2 }

```python
def histogramme(liste: list) -> dict:
    D = dict()
    for e in liste:
        if e in D:
            D[e] = D[e] + 1
        else:
            D[e] = 1
    return D
```

On testera la fonction sur la liste suivante :

```python
L = [ 'alice', 'alice', 'charles', 'charles', 'bob', 'charles', 'denis',
      'edouard', 'denis', 'edouard', 'bob', 'edouard', 'edouard', 'edouard',
      'alice', 'alice', 'charles', 'edouard', 'alice', 'alice', 'bob',
      'charles', 'alice' ]
histogramme(L)
```

Écrire ensuite une fonction `plot_histo` prenant en argument un histogramme (sous la forme d'un dictionnaire) et traçant l'histogramme sous la forme :


    alice  #
    bob    ##

```python
def plot_histo(H: dict) -> None:
    for k in H:
        print(k, "#" * H[k])
```

## Reconstitution d'un itinéraire mélangé

On considère une liste de trajets entre deux villes, telle que

    [ ("Brest", "Quimper"), ("Morlaix", "Guingamp"), ("Rennes", "Nantes"), … ]

Il est garanti qu'un itinéraire unique non cyclique permet de parcourir tous les trajets successivements pour aller d'une ville initiale à une ville finale.

_Cette liste n'est pas ordonnée. Les trajets sont donc dans le désordre. Le problème
consistera justement à reconstituer le bon ordre._

Un bon algorithme consiste à :

  * construire le dictionnaire qui associe les villes d'arrivée aux villes de départ ;
  * construire le dictionnaire inverse du précédent ;
  * parcourir la liste et détecter la ville qui n'est jamais une ville d'arrivée grâce au dictionnaire inverse ;
  * partir de la ville de départ et construire l'itinéraire grâce au dictionnaire de base.

_On constate que cet algorithme a un coût linéaire puisqu'il parcourt quatre fois de suite la liste de départ._

Définir la fonction `itin` prenant en argument une liste de couples et retournant
la liste décrivant l'itinéraire reconstitué.

```python
def itin(l: list) -> list:
    D1, D2 = dict(), dict()
    for a, b in l:
        D1[a], D2[b] = b, a
    for x, _ in l:
        if x not in D2:
            break
    r = [ x ]
    while x in D1:
        x = D1[x]
        r.append(x)
    return r
```
