---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.2'
      jupytext_version: 1.8.1
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

# Arbre couvrant minimal

On cherche ici à construire un [arbre couvrant de poids minimal](https://fr.wikipedia.org/wiki/Arbre_couvrant_de_poids_minimal) pour un graphe connexe pondéré et non orienté. Le travail sera essentiellement tourné vers la résolution du [problème n° 107](https://projecteuler.net/problem=107) du Projet Euler.

On cherche à sélectionner certaines arêtes d'un graphe donné en respectant deux conditions :

  * le graphe résultant doit être un graphe connexe qui conserve tous les sommets de départ ;
  * le poids total du graphe (somme des poids associés aux arêtes) doit être minimal.

Dans un premier temps, on travaillera sur le graphe donné en exemple sur le site du Projet Euler :

![graphe](https://projecteuler.net/project/images/p107_1.png)

représenté par la matrice ci-dessous :

```python
exemple = [
      [  0, 16, 12, 21,  0,  0,  0 ],
      [ 16,  0,  0, 17, 20,  0,  0 ],
      [ 12,  0,  0, 28,  0, 31,  0 ],
      [ 21, 17, 28,  0, 18, 19, 23 ],
      [  0, 20,  0, 18,  0,  0, 11 ],
      [  0,  0, 31, 19,  0,  0, 27 ],
      [  0,  0,  0, 23, 11, 27,  0 ]
    ]
```

où l'on conviendra que le coefficient nul correspond à une absence d'arête (puisque toutes les arêtes considérées ici seront pondérées avec des valeurs strictement positives).

Écrire tout d'abord une fonction qui calcule le poids total du réseau à partir de sa représentation sous la forme d'une matrice d'adjacence :

```python
def network_weight(M: list) -> int:
    s, n = 0, len(M)
    for i in range(n-1):
        for j in range(i+1, n):
            s = s + M[i][j]
    return s
```

Vérifier que le poids total du graphe donné en exemple est bien 243 comme indiqué sur la page du site :

```python
network_weight(exemple)
```

Écrire une fonction convertissant une matrice d'adjacence en une liste de couple `(w, (i, j))` où `w` est le poids de l'arête entre les nœuds `i` et `j` (l'ordre entre les deux coordonnées sera indifférent).

```python
def extract_edges(M: list) -> list:
    L, n = [], len(M)
    for i in range(n-1):
        for j in range(i+1, n):
            if M[i][j] != 0:
                L.append( (M[i][j], (i, j)) )
    return L
```

**Prouver que l'arête de poids minimal appartient nécessairement à l'arbre couvrant de poids minimal.**

**Justifier la correction de l'algorithme suivant :**

  * On trie toutes les arêtes par ordre croissant de poids ;
  * on on retient l'arête de poids minimal comme arête de départ pour la construction de l'arbre couvrant de poids minimal ;
  * on marque comme « accessibles » les deux extrémités (sommets du graphe) de l'arête de poids minimal ;
  * on répète les étapes suivantes jusqu'à ce que toutes les arêtes soient accessibles :

      - repérer l'arête de poids minimal dont une extrémité est déjà accessible et l'autre extrémité ne l'est pas ;
      - on ajoute cette arête à l'arbre couvrant de poids minimal ;
      - on marque comme accessible le sommet rendu accessible grâce à cette arête.

**Calculer la complexité de l'algorithme précédent.**

Implémenter en Python l'algorithme décrit ci-dessus ; la fonction prendra en argument une matrice d'adjacence et retournera la liste des arêtes conservées (selon le format décrit à propos de la fonction `extract_edges`).

```python
def arbre_couvrant_minimal(M: list) -> list:
    n = len(M)
    L = extract_edges(M)
    L.sort()
    nodes = [ False ] * n
    a = L[0]
    T = [ a ]                                   # arête initiale
    nodes[a[1][0]], nodes[a[1][1]] = True, True # marquage des deux noeuds
    for _ in range(n-2):
        for e in L:
            u, v = e[1] # deux extrémités de l'arête considérée
            if nodes[u] and not nodes[v]:
                nodes[v] = True
                T.append(e)
                break
            elif nodes[v] and not nodes[u]:
                nodes[u] = True
                T.append(e)
                break
    return T
```

Vérifier que le résultat du calcul sur l'exemple est le même que sur le site du Projet Euler :

```python
network_weight(exemple) - sum( e[0] for e in arbre_couvrant_minimal(exemple) )
```

Exécuter la cellule suivante de code afin de construire la matrice principale du problème :

```python
data = """
-,-,-,427,668,495,377,678,-,177,-,-,870,-,869,624,300,609,131,-,251,-,-,-,856,221,514,-,591,762,182,56,-,884,412,273,636,-,-,774
-,-,262,-,-,508,472,799,-,956,578,363,940,143,-,162,122,910,-,729,802,941,922,573,531,539,667,607,-,920,-,-,315,649,937,-,185,102,636,289
-,262,-,-,926,-,958,158,647,47,621,264,81,-,402,813,649,386,252,391,264,637,349,-,-,-,108,-,727,225,578,699,-,898,294,-,575,168,432,833
427,-,-,-,366,-,-,635,-,32,962,468,893,854,718,427,448,916,258,-,760,909,529,311,404,-,-,588,680,875,-,615,-,409,758,221,-,-,76,257
668,-,926,366,-,-,-,250,268,-,503,944,-,677,-,727,793,457,981,191,-,-,-,351,969,925,987,328,282,589,-,873,477,-,-,19,450,-,-,-
495,508,-,-,-,-,-,765,711,819,305,302,926,-,-,582,-,861,-,683,293,-,-,66,-,27,-,-,290,-,786,-,554,817,33,-,54,506,386,381
377,472,958,-,-,-,-,-,-,120,42,-,134,219,457,639,538,374,-,-,-,966,-,-,-,-,-,449,120,797,358,232,550,-,305,997,662,744,686,239
678,799,158,635,250,765,-,-,-,35,-,106,385,652,160,-,890,812,605,953,-,-,-,79,-,712,613,312,452,-,978,900,-,901,-,-,225,533,770,722
-,-,647,-,268,711,-,-,-,283,-,172,-,663,236,36,403,286,986,-,-,810,761,574,53,793,-,-,777,330,936,883,286,-,174,-,-,-,828,711
177,956,47,32,-,819,120,35,283,-,50,-,565,36,767,684,344,489,565,-,-,103,810,463,733,665,494,644,863,25,385,-,342,470,-,-,-,730,582,468
-,578,621,962,503,305,42,-,-,50,-,155,519,-,-,256,990,801,154,53,474,650,402,-,-,-,966,-,-,406,989,772,932,7,-,823,391,-,-,933
-,363,264,468,944,302,-,106,172,-,155,-,-,-,380,438,-,41,266,-,-,104,867,609,-,270,861,-,-,165,-,675,250,686,995,366,191,-,433,-
870,940,81,893,-,926,134,385,-,565,519,-,-,313,851,-,-,-,248,220,-,826,359,829,-,234,198,145,409,68,359,-,814,218,186,-,-,929,203,-
-,143,-,854,677,-,219,652,663,36,-,-,313,-,132,-,433,598,-,-,168,870,-,-,-,128,437,-,383,364,966,227,-,-,807,993,-,-,526,17
869,-,402,718,-,-,457,160,236,767,-,380,851,132,-,-,596,903,613,730,-,261,-,142,379,885,89,-,848,258,112,-,900,-,-,818,639,268,600,-
624,162,813,427,727,582,639,-,36,684,256,438,-,-,-,-,539,379,664,561,542,-,999,585,-,-,321,398,-,-,950,68,193,-,697,-,390,588,848,-
300,122,649,448,793,-,538,890,403,344,990,-,-,433,596,539,-,-,73,-,318,-,-,500,-,968,-,291,-,-,765,196,504,757,-,542,-,395,227,148
609,910,386,916,457,861,374,812,286,489,801,41,-,598,903,379,-,-,-,946,136,399,-,941,707,156,757,258,251,-,807,-,-,-,461,501,-,-,616,-
131,-,252,258,981,-,-,605,986,565,154,266,248,-,613,664,73,-,-,686,-,-,575,627,817,282,-,698,398,222,-,649,-,-,-,-,-,654,-,-
-,729,391,-,191,683,-,953,-,-,53,-,220,-,730,561,-,946,686,-,-,389,729,553,304,703,455,857,260,-,991,182,351,477,867,-,-,889,217,853
251,802,264,760,-,293,-,-,-,-,474,-,-,168,-,542,318,136,-,-,-,-,392,-,-,-,267,407,27,651,80,927,-,974,977,-,-,457,117,-
-,941,637,909,-,-,966,-,810,103,650,104,826,870,261,-,-,399,-,389,-,-,-,202,-,-,-,-,867,140,403,962,785,-,511,-,1,-,707,-
-,922,349,529,-,-,-,-,761,810,402,867,359,-,-,999,-,-,575,729,392,-,-,388,939,-,959,-,83,463,361,-,-,512,931,-,224,690,369,-
-,573,-,311,351,66,-,79,574,463,-,609,829,-,142,585,500,941,627,553,-,202,388,-,164,829,-,620,523,639,936,-,-,490,-,695,-,505,109,-
856,531,-,404,969,-,-,-,53,733,-,-,-,-,379,-,-,707,817,304,-,-,939,164,-,-,616,716,728,-,889,349,-,963,150,447,-,292,586,264
221,539,-,-,925,27,-,712,793,665,-,270,234,128,885,-,968,156,282,703,-,-,-,829,-,-,-,822,-,-,-,736,576,-,697,946,443,-,205,194
514,667,108,-,987,-,-,613,-,494,966,861,198,437,89,321,-,757,-,455,267,-,959,-,616,-,-,-,349,156,339,-,102,790,359,-,439,938,809,260
-,607,-,588,328,-,449,312,-,644,-,-,145,-,-,398,291,258,698,857,407,-,-,620,716,822,-,-,293,486,943,-,779,-,6,880,116,775,-,947
591,-,727,680,282,290,120,452,777,863,-,-,409,383,848,-,-,251,398,260,27,867,83,523,728,-,349,293,-,212,684,505,341,384,9,992,507,48,-,-
762,920,225,875,589,-,797,-,330,25,406,165,68,364,258,-,-,-,222,-,651,140,463,639,-,-,156,486,212,-,-,349,723,-,-,186,-,36,240,752
182,-,578,-,-,786,358,978,936,385,989,-,359,966,112,950,765,807,-,991,80,403,361,936,889,-,339,943,684,-,-,965,302,676,725,-,327,134,-,147
56,-,699,615,873,-,232,900,883,-,772,675,-,227,-,68,196,-,649,182,927,962,-,-,349,736,-,-,505,349,965,-,474,178,833,-,-,555,853,-
-,315,-,-,477,554,550,-,286,342,932,250,814,-,900,193,504,-,-,351,-,785,-,-,-,576,102,779,341,723,302,474,-,689,-,-,-,451,-,-
884,649,898,409,-,817,-,901,-,470,7,686,218,-,-,-,757,-,-,477,974,-,512,490,963,-,790,-,384,-,676,178,689,-,245,596,445,-,-,343
412,937,294,758,-,33,305,-,174,-,-,995,186,807,-,697,-,461,-,867,977,511,931,-,150,697,359,6,9,-,725,833,-,245,-,949,-,270,-,112
273,-,-,221,19,-,997,-,-,-,823,366,-,993,818,-,542,501,-,-,-,-,-,695,447,946,-,880,992,186,-,-,-,596,949,-,91,-,768,273
636,185,575,-,450,54,662,225,-,-,391,191,-,-,639,390,-,-,-,-,-,1,224,-,-,443,439,116,507,-,327,-,-,445,-,91,-,248,-,344
-,102,168,-,-,506,744,533,-,730,-,-,929,-,268,588,395,-,654,889,457,-,690,505,292,-,938,775,48,36,134,555,451,-,270,-,248,-,371,680
-,636,432,76,-,386,686,770,828,582,-,433,203,526,600,848,227,616,-,217,117,707,369,109,586,205,809,-,-,240,-,853,-,-,-,768,-,371,-,540
774,289,833,257,-,381,239,722,711,468,933,-,-,17,-,-,148,-,-,853,-,-,-,-,264,194,260,947,-,752,147,-,-,343,112,273,344,680,540,-
"""
data2 = data.strip("\r\n ").replace("-","0").split("\n")
data3 = [ d.split(",") for d in data2 ]
matrice = [ [ int(k) for k in r ] for r in data3 ]
```

Résoudre le problème n° 107 du Projet Euler.

```python
network_weight(matrice) - sum( e[0] for e in arbre_couvrant_minimal(matrice) )
```
