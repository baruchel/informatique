---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.2'
      jupytext_version: 1.8.1
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

# Le tri par sélection

Le tri par sélection est un tri médiocre mais proche de l'intuition humaine
(si nous devons trier « à la main » une vingtaine d'objets, il arrive assez
souvent que nous procédions de la sorte). L'idée est la suivante : prendre à
chaque fois le plus petit élément non encore trié et l'ajouter à la suite de
ce qui a déjà été trié.


## Version intuitive

Exemple pour la liste de départ [11,23,7,4] :

    Liste triée : []            Liste non triée : [11,23,7,4] (minimum: 4)
                  [4]                             [11,23,7]   (minimum: 7)
                  [4,7]                           [11,23]     (minimum: 11)
                  [4,7,11]                        [23]        (minimum: 23)

Écrire une fonction `tri_sélection` effectuant un tel tri sur une liste donné
et retournant la liste triée. On s'autorisera ici à supprimer l'élément de la
liste initiale à chaque fois. La fonction _retournera_ une (nouvelle) liste
triée.

```python
def tri_selection1(liste: list) -> list:
    out = []
    while len(liste) > 0:
        m, k = liste[0], 0 # valeur et position du minimum
        for i in range(1, len(liste)):
            if liste[i] < m:
                m, k = liste[i], i
        out.append(liste.pop(k))
    return out

print(tri_selection1([11, 23, 7, 4]))
```

```python
# tester le code précédent
from random import randrange
L = [randrange(0, 100) for _ in range(20)]
print(L)
print(tri_selection1(L))
```


## Version en place

Deux problèmes se posent avec la version précédente :

  * la liste de départ est détruite ;
  * la suppression d'un élément à chaque tour a un coût significatif.

On codera donc une variante _en place_ qui procède par échange : on échange
le premier élément de la liste avec le plus petit élément de la liste, puis
on échange le deuxième avec le plus petit restant, puis le troisième, etc.

![tri sélection](https://user-images.githubusercontent.com/36489953/42171344-5554d9d2-7e19-11e8-8537-7811ebbbd1b6.gif)

Exemple pour la liste de départ [11,23,7,4] :

    Liste de travail : [11,23,7,4]
                       [4,23,7,11] # après échange de 11 et de 4
                       [4,7,23,11] # après échange de 23 et de 7
                       [4,7,11,23] # après échange de 23 et de 11

On conseille fortement ici l'utilisation d'une fonction auxiliaire `minindex_from_index`
qui retourne l'indice de l'élément minimal à partir d'un certain indice ; on écrira
donc en premier cette fonction :

```python
def minindex_from_index(liste: list, ind: int) -> int:
    m, k = liste[ind], ind
    for i in range(ind+1, len(liste)):
        if liste[i] < m:
            m, k = liste[i], i
    return k
```

On pourra alors écrire la version en place de l'algorithme. Observer la
différence de signature de la fonction suivante (typique d'une fonction en
place) :

```python
def tri_selection2(liste: list) -> None:
    for k in range(len(liste)):
        i = minindex_from_index(liste, k)
        liste[k], liste[i] = liste[i], liste[k]
```

Cette version en place s'utilise différemment de la précédente (elle ne retourne rien) :

```python
# tester le code précédent
from random import randrange
L = [randrange(0, 100) for _ in range(20)]
print(L)
tri_selection2(L)
print(L)
```

L'animation suivante montre trois cas distincts qui ont le même coût temporel pour le tri sélection :

| liste aléatoire | liste presque triée | liste à l'envers |
|:---:|:---:|:---:|
| ![img](https://bs-uploads.toptal.io/blackfish-uploads/sorting_algorithms_page/content/illustration/animated_image_file/animated_image/27809/selection-sort-f03ab6c4bee12d931a4899d1d48254fe.gif) | ![img](https://bs-uploads.toptal.io/blackfish-uploads/sorting_algorithms_page/content/illustration/animated_image_file/animated_image/27817/selection-sort-316f7c1ef1da5e8bcea0499e7102692e.gif) | ![img](https://bs-uploads.toptal.io/blackfish-uploads/sorting_algorithms_page/content/illustration/animated_image_file/animated_image/27825/selection-sort-5a466b70273c580220bdf7478dc43b05.gif) |


## Problème de la stabilité du tri

On rappelle qu'un tri stable préserve l'ordre initial d'éléments égaux (par exemple,
si l'on tri des élèves selon une note scolaire, deux élèves ayant reçu la même note
devront conserver leur ordre relatif initial).

Construire un contre-exemple simple constitué de quelques couples `(prénom, note)`
prouvant la non stabilité du tri par sélection _en place_.

Construire le contre-exemple minimal (en nombre d'éléments) et justifier.
