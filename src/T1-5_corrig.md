---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.2'
      jupytext_version: 1.8.1
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

# Travail sur une liste avec deux boucles imbriquées

## Recherche d'une sous-chaîne de caractères

Écrire une fonction qui retourne l'indice du début de la sous-chaîne `w` dans la chaîne de caractères `s` et retourne conventionnellement la longueur de la chaîne de caractères si la sous-chaîne `w` n'apparaît pas dans `s`. Par exemple :

    substring("aujourd'hui", "jour")  -->  2
    substring("aujourd'hui", "nuit")  --> 11

```python
def substring(s: str, w:str) -> int:
    n, m = len(s), len(w)
    for i in range(n-m+1):
        c = 0
        while s[i+c] == w[c]:
            c = c + 1
            if c == m: return i
    return n

print(substring("aujourd'hui", "jour"))
print(substring("aujourd'hui", "nuit"))
print(substring("aujourd'hui", "hui"))
```

## Construction du crible d'Ératosthène

Une méthode classique pour construire la liste des nombres premiers jusqu'à un entier donné est
fournie par le [crible d'Ératosthène](https://fr.wikipedia.org/wiki/Crible_d%27%C3%89ratosth%C3%A8ne) (le lien précédent donne d'ailleurs l'algorithme sous la forme de pseudo-code).

Cet algorithme repose sur deux boucles imbriquées travaillant sur une seule liste.

Écrire une fonction `primes` prenant en argument un entier `n` et retournant la liste des nombres premiers inférieurs ou égaux à `n`.

```python
def primes(n: int) -> list:
    p = [ True ] * (n+1)
    p[0], p[1] = False, False
    out = []
    for k in range(2, n+1):
        if p[k]:
            out.append(k)
            for i in range(k*k, n+1, k): p[i] = False
    return out

print(primes(100))
```

À l'aide de la fonction précédente, résoudre le [problème n° 3](https://projecteuler.net/problem=3) du _Projet Euler_ :

> Problem 3
>
> The prime factors of 13195 are 5, 7, 13 and 29.
>
> What is the largest prime factor of the number 600851475143 ?

```python
# Le nombre est trop élevé pour que l'on recherche tous les nombres premiers
# jusqu'à cet ordre de grandeur, MAIS un nombre composé admet au moins un facteur
# premier plus petit que sa racine carrée (trivial). On va donc dans un premier
# temps simplifier le problème en recherchant tous les facteurs premiers
# inférieurs à la racine carrée :
def semi_factorize(n):
    f = []
    s = int(n**.5) # racine carrée
    for k in primes(s):
        while n%k == 0:
            n = n // k
            f.append(k)
    return f

print(semi_factorize(600851475143))

# Le résultat est alors : [71, 839, 1471, 6857]
# En fait la factorisation s'avère complète (ce n'était pas certain à l'avance) :
# 
#    71 * 839 * 1471 * 6857 = 600851475143
# 
# Le problème est donc résolu !
```
