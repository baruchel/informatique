---
jupyter:
  jupytext:
    text_representation:
      extension: .md
      format_name: markdown
      format_version: '1.2'
      jupytext_version: 1.8.1
  kernelspec:
    display_name: Python 3
    language: python
    name: python3
---

# T2-4 Algorithmes gloutons (suite)

L'un des multiples rôles des systèmes d'exploitation, sur un ordinateur, est
l'allocation de blocs de mémoire contiguë aux processus qui en font la demande :

  * quand un processus démarre, il a besoin d'une certaine quantité de mémoire
    pour pouvoir lui-même démarrer ;
  * quand un processus manipule des données, il a besoin d'espace en mémoire
    pour les stocker.

Or, un système moderne faisant tourner de très nombreux processus qui démarrent
puis éventuellement s'arrêtent se trouve confronté à un problème de fragmentation
de la mémoire : au lieu de disposer librement de toute la mémoire disponible,
il dispose plutôt d'une liste de blocs libres de tailles très variées, par
exemple :

    Blocs disponibles :     [ 100, 30, 600, 325, 200, 120, 260 ]

On s'intéressera ici à l'algorithme de choix d'un bloc de mémoire libre
lorsqu'un processus demande une quantité $k$ de mémoire.

Supposons qu'un processus demande une quantité 110 de mémoire dans l'exemple
ci-dessus, le système a le choix entre cinq possibilités, et dans tous les
cas, il subsistera un bloc plus petit dont il pourra continuer de disposer.

Bien entendu, le système ignore quelle sera la demande suivante ; il ne
peut donc pas faire d'évaluation globale : les demandes sont traitées successivement
au cas par cas.


## Quatre stratégies envisagées :

On rappelle que l'algorithme glouton repose sur un critère de choix (qui permet
en général de trier les objets disponibles). On expose ici quatre stratégies
possibles :

  * algorithme _first fit_ (il n'y a pas ici de réel critère de tri ; on laisse
    jouer le hasard et l'on attribue le premier bloc suffisamment large) ;
  * algorithme _best fit_ (on sélectionne le plus petit bloc capable de répondre
    à la demande) ;
  * algorithme _worst fit_ (on sélectionne le plus grand bloc possible) ;
  * algorithme _next fit_ (comme le _first fit_ sauf que la sélection ne
    repart pas du début de la liste à chaque demande mais continue où elle
    en était restée la dernière fois à partir du bloc suivant).

On suppose que les demandes suivantes surgissent ; indiquez comment chaque scénario
se déroule (en reprenant les blocs de l'exemple ci-dessus) ; on s'arrêtera pour
chaque scénario au premier échec rencontré :

    Blocs disponibles :     [ 100, 30, 600, 325, 200, 120, 260 ]
    Demandes successives :  [ 110, 300, 350, 225, 210, 200, 125 ]

_(Dans la pratique, il ne faut pas oublier que les blocs sont parfois libérés rapidement
et que certains scénarios apparemment médiocres peuvent en tirer parti.)_


## Codage des quatre algorithmes

Écrire quatre fonctions `first_fit`, `best_fit`, `worst_fit`, `next_fit`, prenant
en argument deux listes, l'une pour les blocs disponibles, l'autre pour les
demandes successives, et affichant l'état de la mémoire après chaque allocation,
jusqu'au premier échec rencontré.

La fonction retournera `True` si elle est parvenue à satisfaire toutes les demandes
et `False` dans le cas contraire.

```python
def first_fit(blocs: list, demandes: list) -> bool:
    success = True
    for d in demandes:
        success = False
        for i in range(len(blocs)):
            if d <= blocs[i]:           # allocation possible
                blocs[i] = blocs[i] - d # reste disponible
                success = True          # allocation réussie
                break                   # sortie de la boucle
        if not success: break # aucun bloc aloué
    print("État de la mémoire :", blocs)
    return success
```

```python
def best_fit(blocs: list, demandes: list) -> bool:
    success = True
    for d in demandes:
        success = False
        if len(blocs) == 0:
            break
        mybest = -1
        mydelta = float("inf")
        for i in range(len(blocs)):
            if d <= blocs[i]:
                delta = blocs[i] - d
                if delta <= mydelta:
                    mydelta, mybest = delta, i
                    success = True # allocation réussie
        if not success: break # aucun bloc aloué
        blocs[mybest] = mydelta
    print("État de la mémoire :", blocs)
    return success
```

```python
def worst_fit(blocs: list, demandes: list) -> bool:
    success = True
    for d in demandes:
        success = False
        if len(blocs) == 0:
            break
        mybest = -1
        mydelta = -1
        for i in range(len(blocs)):
            if d <= blocs[i]:
                delta = blocs[i] - d
                if delta >= mydelta:
                    mydelta, mybest = delta, i
                    success = True # allocation réussie
        if not success: break # aucun bloc aloué
        blocs[mybest] = mydelta
    print("État de la mémoire :", blocs)
    return success
```

```python
def next_fit(blocs: list, demandes: list) -> bool:
    success = True
    j = 0
    for d in demandes:
        success = False
        for i in range(j, j + len(blocs)):
            ind = i % len(blocs)
            if d <= blocs[ind]:             # allocation possible
                blocs[ind] = blocs[ind] - d # reste disponible
                success = True              # allocation réussie
                j = ind + 1                 # mise à jour du pointeur
                break                       # sortie de la boucle
        if not success: break # aucun bloc aloué
    print("État de la mémoire :", blocs)
    return success
```
