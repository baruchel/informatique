{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "074bf104",
   "metadata": {},
   "source": [
    "# Recherche d'un élément dans une collection de données\n",
    "\n",
    "La localisation de données est un problème fondamental de l'informatique\n",
    "moderne (par exemple quand on cherche à retrouver les informations liées à\n",
    "un compte).\n",
    "\n",
    "Or cette recherche a un _coût temporel_. Le but de ce cours est de présenter\n",
    "trois approches pour effectuer cette recherche :\n",
    "\n",
    "  * recherche dans une liste par itération simple ;\n",
    "  * recherche dans une liste triée par dichotomie ;\n",
    "  * recherche d'informations à l'aide d'un tableau associatif."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "94de5ccc",
   "metadata": {},
   "source": [
    "## Recherche d'un élément dans une liste\n",
    "\n",
    "La liste est le type fondamental en Python permettant de stocker des données\n",
    "homogènes. Ces données ont des indices, et l'on itère naturellement sur elles\n",
    "en suivant l'ordre des indices :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "19caa106",
   "metadata": {},
   "outputs": [],
   "source": [
    "L = [ \"Paris\", \"Brest\", \"Nice\", \"Lyon\", \"Bordeaux\", \"Marseille\", \"Strasbourg\" ]\n",
    "for e in L:\n",
    "    print(e)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2cfe906c",
   "metadata": {},
   "source": [
    "L'indice peut être explicitement utilisé pour le parcours de la liste :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bfa1d3f1",
   "metadata": {},
   "outputs": [],
   "source": [
    "L = [ \"Paris\", \"Brest\", \"Nice\", \"Lyon\", \"Bordeaux\", \"Marseille\", \"Strasbourg\" ]\n",
    "for i in range(len(L)):\n",
    "    print(L[i])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "25c5a409",
   "metadata": {},
   "source": [
    "Le test d'appartenance s'effectue à l'aide du mot-clé `in`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "354a1164",
   "metadata": {},
   "outputs": [],
   "source": [
    "L = [ \"Paris\", \"Brest\", \"Nice\", \"Lyon\", \"Bordeaux\", \"Marseille\", \"Strasbourg\" ]\n",
    "if \"Quimper\" in L:\n",
    "    print(\"'Quimper' est dans la liste !\")\n",
    "else:\n",
    "    print(\"'Quimper' n'est pas dans la liste !\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "39de0902",
   "metadata": {},
   "source": [
    "### Analyse du test d'appartenance\n",
    "\n",
    "Le test d'appartenance peut être reconstitué de la façon suivante :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "abe74fea",
   "metadata": {},
   "outputs": [],
   "source": [
    "L = [ \"Paris\", \"Brest\", \"Nice\", \"Lyon\", \"Bordeaux\", \"Marseille\", \"Strasbourg\" ]\n",
    "result = False\n",
    "for e in L:\n",
    "    if e == \"Quimper\":\n",
    "        result = True\n",
    "        break\n",
    "if result == True:\n",
    "    print(\"'Quimper' est dans la liste !\")\n",
    "else:\n",
    "    print(\"'Quimper' n'est pas dans la liste !\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0a608fb7",
   "metadata": {},
   "source": [
    "ou de façon plus élégante à l'aide d'une fonction :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "76f1169e",
   "metadata": {},
   "outputs": [],
   "source": [
    "from typing import List, Any # Annotations de type\n",
    "\n",
    "def test(elmt: Any, liste: List[Any]) -> bool:\n",
    "    for e in liste:\n",
    "        if e == elmt:\n",
    "            return True\n",
    "    return False\n",
    "\n",
    "L = [ \"Paris\", \"Brest\", \"Nice\", \"Lyon\", \"Bordeaux\", \"Marseille\", \"Strasbourg\" ]\n",
    "if test(\"Quimper\", L):\n",
    "    print(\"'Quimper' est dans la liste !\")\n",
    "else:\n",
    "    print(\"'Quimper' n'est pas dans la liste !\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a2124446",
   "metadata": {},
   "source": [
    "_(On note ci-dessus que l'instruction `break` n'est plus nécessaire puisque le\n",
    "`return` met immédiatement fin à l'exécution de la fonction.)_\n",
    "\n",
    "Le code précédent montre :\n",
    "\n",
    "  * que dans le pire des cas, toute la liste doit être parcourue ;\n",
    "  * que l'itération s'arrête à la première occurrence de l'élément recherché.\n",
    "\n",
    "On dit alors que ce test a un _coût linéaire_ (car le temps de calcul est\n",
    "proportionnel à la taille de la liste, dans le pire des cas)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "eb40eb32",
   "metadata": {},
   "source": [
    "## Recherche d'un élément dans une liste ordonnée (triée)\n",
    "\n",
    "Quand la liste est triée, la recherche est bien plus rapide grâce à un\n",
    "algorithme fondamental, la _dichotomie_.\n",
    "\n",
    "L'idée est de travailler sur un intervalle et de réduire ce dernier de\n",
    "moitié à chaque itération : comme la liste est triée, on sait si l'élément\n",
    "recherché doit se trouver à gauche ou à droite du milieu de l'intervalle.\n",
    "\n",
    "La version suivante définit deux bornes `a` et `b` (ce sont des indices)\n",
    "qui délimitent un intervalle fermé _dans lequel l'élément recherché est\n",
    "susceptible de se trouver_ :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c070b776",
   "metadata": {},
   "outputs": [],
   "source": [
    "from typing import List, Any # Annotations de type\n",
    "\n",
    "def test_dicho(elmt: Any, liste: List[Any]) -> bool:\n",
    "    a, b = 0, len(liste)-1 # intervalle (fermé) de travail\n",
    "    while a <= b:\n",
    "        c = (a+b)//2\n",
    "        if liste[c] < elmt:\n",
    "            a = c + 1 \n",
    "        elif liste[c] > elmt:\n",
    "            b = c - 1 \n",
    "        else:\n",
    "            return True\n",
    "    return False"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4a17b315",
   "metadata": {},
   "source": [
    "On voit dans le code ci-dessus que l'intervalle de travail est divisé par 2\n",
    "à chaque itération ; un tel processus a un _coût logarithmique_ (c'est un\n",
    "algorithme extrêmement utilisé, mais il suppose que la liste soit maintenue\n",
    "de façon intelligente afin de rester constamment triée, y compris quand on\n",
    "lui ajoute ou retire des éléments)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "605bea10",
   "metadata": {},
   "source": [
    "## Recherche d'informations à l'aide d'un tableau associatif\n",
    "\n",
    "Le problème des deux codes précédents est que leur coût temporel dépend\n",
    "de la taille de la liste. Certaines structures (par ailleurs plus complexes\n",
    "dans leur fonctionnement) permettent une recherche à _coût constant_.\n",
    "\n",
    "L'idée est d'associer à chaque valeur un indice construit à partir de la valeur\n",
    "elle-même (par exemple pour le mot \"Brest\", on pourrait sommer la valeur\n",
    "numérique de chaque lettre).\n",
    "\n",
    "De façon plus rigoureuse, on construit une _surjection_ d'un ensemble défini\n",
    "par un certain type de données (par exemple les chaînes de caractères) vers\n",
    "une partie de N majorée par un entier suffisamment petit (pour ne pas gaspiller\n",
    "trop de mémoire) ; on peut alors aisément disposer d'un indice permettant\n",
    "de vérifier _immédiatement_ si des données sont disponibles.\n",
    "\n",
    "_(Dans la pratique, il faut aussi gérer le problème des collisions car plusieurs\n",
    "objets peuvent avoir la même image par la surjection retenue, mais on estime que\n",
    "les solutions à ce problème restent compatibles avec un coût constant.)_\n",
    "\n",
    "Un type de données particulièrement utile propose ce genre de structure en Python,\n",
    "le _dictionnaire_ :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "de0458a5",
   "metadata": {},
   "outputs": [],
   "source": [
    "    D = { \"alfred\": 42, \"Berthe\": 37, \"didier\": 18, \"camille\": 58 }\n",
    "    # quelle que soit la taille du dictionnaire, les lignes suivantes\n",
    "    # ont un coût temporel constant :\n",
    "    if \"jeanne\" in D: print(\"Jeanne :\", D[\"jeanne\"])\n",
    "    if \"didier\" in D: print(\"Didier :\", D[\"didier\"])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "931e7108",
   "metadata": {},
   "source": [
    "### Problème : compter les éléments distincts d'une liste\n",
    "\n",
    "On cherche ici à compter les éléments d'une liste sans tenir compte des\n",
    "répétitions (ainsi `[3, 2, 5, 1, 2]` possède 4 éléments distincts). On utilisera\n",
    "alors un dictionnaire pour se souvenir des éléments déjà rencontrés :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "27ac0783",
   "metadata": {},
   "outputs": [],
   "source": [
    "from typing import List, Any # Annotations de type\n",
    "\n",
    "def distinct_count(liste: List[Any]) -> int:\n",
    "    s = 0 \n",
    "    elmts = dict()\n",
    "    for e in liste:\n",
    "        if e not in elmts:\n",
    "            s = s + 1 \n",
    "            elmts[e] = True\n",
    "    return s"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
