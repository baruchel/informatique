{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "00b7aa3e",
   "metadata": {},
   "source": [
    "# Le tri par insertion\n",
    "\n",
    "Le tri par insertion est un tri fréquemment utilisé ; il est simple à programmer,\n",
    "en place, stable. Il est assez médiocre dans le cas général, mais très adapté\n",
    "à un certain nombre de situations en adéquation avec des besoins réels (auquel\n",
    "cas il peut atteindre un coût linéaire).\n",
    "\n",
    "Même dans le cas général, son coût est quadratique avec une constante\n",
    "multiplicative faible, ce qui le rend plus rapide que des tris sophistiqués pour\n",
    "des petites tailles de liste.\n",
    "\n",
    "Ce tri s'illustre facilement à partir de la référence à un jeu de cartes : on\n",
    "parcourt un jeu de cartes, en prenant à chaque fois la carte suivante et en\n",
    "l'insérant à sa place parmi les cartes déjà triées.\n",
    "\n",
    "![tri sélection](https://user-images.githubusercontent.com/36489953/42171484-b508016a-7e19-11e8-8d47-3b95d788d579.gif)\n",
    "\n",
    "Si la liste de départ est [11,23,7,4], le processus se fera ainsi :\n",
    "\n",
    "    [11,23,7,4]  # la première valeur est triée (le 11)\n",
    "    [11,23,7,4]  # les deux premières valeurs sont triées (11,23)\n",
    "    [7,11,23,4]  # les trois premières valeurs sont triées (7,11,23)\n",
    "    [4,7,11,23]  # toute la liste est triée\n",
    "\n",
    "Une boucle parcourt donc la liste de gauche à droite, puis à chaque fois revient\n",
    "en arrière en _décalant_ les valeurs plus grande vers la droite afin de faire une\n",
    "place pour la valeur à insérer. On illustre la phase d'insertion en reprenant le\n",
    "passage de la ligne 2 à la ligne 3 dans l'exemple ci-dessus :\n",
    "\n",
    "    [11,23,7,4]  # les deux premières valeurs sont triées (11,23)\n",
    "      --> on arrive à la valeur 7 que l'on garde en mémoire\n",
    "      --> on décale le 23 vers la droite\n",
    "    [11,23,23,4] # étape intermédiaire  (7 est en mémoire)\n",
    "      --> on décale le 11 vers la droite\n",
    "    [11,11,23,4] # étape intermédiaire  (7 est en mémoire)\n",
    "      --> on insère finalement la valeur gardée en mémoire\n",
    "    [7,11,23,4]  # les trois premières valeurs sont triées (7,11,23)\n",
    "\n",
    "Il faut faire attention à la condition d'arrêt de la boucle intérieure (celle\n",
    "qui va de la droite vers la gauche), car deux situations peuvent se présenter :\n",
    "\n",
    "  * la boucle intérieure prend fin quand elle rencontre un élément plus petit\n",
    "    que celui gardé en mémoire ;\n",
    "  * la boucle intérieure prend fin quand elle arrive en début de liste.\n",
    "\n",
    "L'oubli du second cas peut mener à certains problèmes."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bb9918d0",
   "metadata": {},
   "source": [
    "## Codage du tri par insertion\n",
    "\n",
    "Écrire une fonction `tri_insertion` triant en place une liste de valeurs ; la\n",
    "signature de la fonction ci-dessous est typique d'une fonction en place :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "de00bca6",
   "metadata": {},
   "outputs": [],
   "source": [
    "def tri_insertion(liste: list) -> None:\n",
    "    n = len(liste)\n",
    "    for i in range(1, n):\n",
    "        x = liste[i]\n",
    "        j = i\n",
    "        while j > 0 and liste[j-1] > x:\n",
    "            liste[j] = liste[j-1]\n",
    "            j = j - 1\n",
    "        liste[j] = x"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "47d9fd8d",
   "metadata": {},
   "source": [
    "Tester le code sur une liste aléatoire :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "887a972a",
   "metadata": {},
   "outputs": [],
   "source": [
    "from random import randrange\n",
    "\n",
    "L = [randrange(0, 100) for _ in range(80)]\n",
    "print(L)\n",
    "tri_insertion(L)\n",
    "print(L)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "460b94e9",
   "metadata": {},
   "source": [
    "L'animation suivante montre trois cas distincts (cas moyen, meilleur cas, pire cas) :\n",
    "\n",
    "| liste aléatoire | liste presque triée | liste à l'envers |\n",
    "|:---:|:---:|:---:|\n",
    "| ![img](https://bs-uploads.toptal.io/blackfish-uploads/sorting_algorithms_page/content/illustration/animated_image_file/animated_image/27777/insertion-sort-da381c7f50e4edcaf1a468d5255b666b.gif) | ![img](https://bs-uploads.toptal.io/blackfish-uploads/sorting_algorithms_page/content/illustration/animated_image_file/animated_image/27785/insertion-sort-93cefb274b4367139eb1b92904b9cd96.gif) | ![img](https://bs-uploads.toptal.io/blackfish-uploads/sorting_algorithms_page/content/illustration/animated_image_file/animated_image/27793/insertion-sort-df4ad231cb2cd5c68d0852a6277bf12f.gif) |"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d9b0ee75",
   "metadata": {},
   "source": [
    "## Exercices\n",
    "\n",
    "**Exercice** Quel est le cas le plus favorable pour ce tri ? Justifier.\n",
    "\n",
    "**Exercice** Quel est le pire cas pour ce tri ? Justifier.\n",
    "\n",
    "S'il reste du temps, reprendre le code ci-dessus, le modifier pour compter le\n",
    "nombre de comparaisons, et illustrer les deux réponses précédentes en affichant\n",
    "le nombre de comparaisons effectuées pour les deux cas extrêmes."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
