{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Résolution du problème du Sudoku\n",
    "\n",
    "## implémentation d'un algorithme de backtracking à l'aide d'une pile\n",
    "\n",
    "_Le présent T.D. s'intéresse à la notion fondamentale de pile. Le langage Python dispose d'une structure de liste extrêmement adaptée pour représenter les piles._\n",
    "\n",
    "Le problème du [Sudoku](https://fr.wikipedia.org/wiki/Sudoku) se résout classiquement en [empilant](https://fr.wikipedia.org/wiki/Pile_%28informatique%29) des hypothèses : si l'on met telle valeur dans telle case, alors telle autre case ne peut contenir que telle ou telle valeur, etc. Les problèmes faciles se bornent parfois à un seul niveau dans la pile des hypothèses (telle case ne peut contenir qu'une seule valeur possible) ; des problèmes plus complexes supposent que l'on empile deux ou trois hypothèses avant de tomber sur un blocage qui invalide les hypothèses précédentes.\n",
    "\n",
    "Si l'on empile des hypothèses de valeurs possibles et que l'on découvre une case pour laquelle aucune hypothèse ne peut être faite, il faut dépiler les hypothèses _en partant du haut de la pile_ (c'est-à-dire des dernières hypothèses faites) jusqu'à retrouver une possibilité qui n'a pas encore été explorée. Ce processus de retour en arrière s'appelle le [backtracking](https://fr.wikipedia.org/wiki/Retour_sur_trace)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Les piles en Python\n",
    "\n",
    "Le langage Python permet d'implémenter simplement des [piles](https://fr.wikipedia.org/wiki/Pile_%28informatique%29) à l'aide de listes ; une liste vide représente une pile vide, et l'on empile ou dépile des objets à l'aide des deux méthodes `append` et `pop` :\n",
    "\n",
    "    >>> stack = []        # on initialise une pile vide\n",
    "    >>> stack.append(12)  # on empile la valeur 12\n",
    "    >>> stack.append(42)  # on empile la valeur 42\n",
    "    >>> stack.pop()       # on dépile la dernière valeur empilée (donc ici 42)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Le problème retenu\n",
    "\n",
    "On prendra ici le problème suivant :\n",
    "\n",
    "![puzzle](http://baruchel.metapath.org/classes/prepas/informatique/td_sudoku/sudoku.png)\n",
    "\n",
    "(pris sur la page [http://www.sudokuwiki.org/Weekly_Sudoku.asp?puz=28](http://www.sudokuwiki.org/Weekly_Sudoku.asp?puz=28) et considéré comme extrêmement difficile dans le classement de la page [http://www.sudokuwiki.org/arto_inkala_sudoku](http://www.sudokuwiki.org/arto_inkala_sudoku)).\n",
    "\n",
    "On représentera cette grille de façon classique à l'aide de la variable globale suivante :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "G = [[ 6, 0, 0,    0, 0, 8,    9, 4, 0 ],\n",
    "     [ 9, 0, 0,    0, 0, 6,    1, 0, 0 ],\n",
    "     [ 0, 7, 0,    0, 4, 0,    0, 0, 0 ],\n",
    "        \n",
    "     [ 2, 0, 0,    6, 1, 0,    0, 0, 0 ],\n",
    "     [ 0, 0, 0,    0, 0, 0,    2, 0, 0 ],\n",
    "     [ 0, 8, 9,    0, 0, 2,    0, 0, 0 ],\n",
    "        \n",
    "     [ 0, 0, 0,    0, 6, 0,    0, 0, 5 ],\n",
    "     [ 0, 0, 0,    0, 0, 0,    0, 3, 0 ],\n",
    "     [ 8, 0, 0,    0, 0, 1,    6, 0, 0 ]]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Identifier les hypothèses possibles pour une case donnée\n",
    "\n",
    "Écrire une fonction `candidates` prenant trois arguments, une grille et deux coordonnées $x$ et $y$. La fonction retourne la liste, éventuellement vide, de toutes les valeurs possibles pour cette case. On appelle _valeur possible_ une valeur ne transgressant de façon directe aucune des trois règles usuelles du jeu (aucun doublon dans la même ligne, dans la même colonne ou dans le même bloc carré). _On ne se préoccupe pas d'une éventuelle transgression impliquée ultérieurement par telle ou telle valeur ; on se contente d'éviter ici les transgressions qualifiées de « directes »._\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def candidates(grid, x, y):\n",
    "    C = [True] * 10\n",
    "    for k in range(9):\n",
    "        C[grid[k][x]] = False # colonne\n",
    "        C[grid[y][k]] = False # ligne\n",
    "    bx, by = (x//3)*3, (y//3)*3\n",
    "    for kx in range(bx, bx+3):\n",
    "        for ky in range(by, by+3):\n",
    "            C[grid[ky][kx]] = False\n",
    "    return [ k for k in range(1, 10) if C[k] ]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Normalement, la fonction précédente doit se comporter comme suit :\n",
    "\n",
    "    >>> candidates(G, 0, 2)  # case la plus à gauche sur la troisième ligne en partant du haut\n",
    "    [1, 3, 5]\n",
    "    \n",
    "    >>> candidates(G, 1, 0)  # troisième case en partant de la gauche sur la ligne du haut\n",
    "    [1, 2, 3, 5]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Effectuer quelques tests\n",
    "\n",
    "candidates(G, 0, 2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Repérer la case la plus intéressante\n",
    "\n",
    "Une résolution optimale du problème suppose que l'on fasse des hypothèses sur les cases les plus contraintes, c'est-à-dire celles pour lesquelles le moins d'hypothèses peuvent être faites. _Une case pour laquelle aucune hypothèse ne peut être faite est d'autant plus intéressante qu'elle met fin à une série d'hypothèse et provoque le backtracking._\n",
    "\n",
    "Écrire une fonction `min_cell` prenant en argument une grille et retournant l'emplacement et les valeurs possibles d'une case pour laquelle le nombre d'hypothèses est minimal (en cas d'égalité, n'importe laquelle des cases concernées pourra être retournée). Bien entendu, une case sans hypothèse possible peut être retournée le cas échéant. Les trois valeurs seront retournées dans l'ordre : abscisse, ordonnée, hypothèses."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def min_cell(grid):\n",
    "    mymin, cmin, xmin, ymin = 10, [], -1, -1\n",
    "    for kx in range(9):\n",
    "        for ky in range(9):\n",
    "            if grid[ky][kx] != 0: continue\n",
    "            c = candidates(grid, kx, ky)\n",
    "            if len(c) < mymin:\n",
    "                mymin, cmin, xmin, ymin = len(c), c, kx, ky\n",
    "    return xmin, ymin, cmin"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Le backtracking\n",
    "\n",
    "On suppose qu'il existe une pile semblable à ceci (à lire de bas en haut) :\n",
    "\n",
    "    (5, 2, [3, 9])  ← haut de la pile\n",
    "    (0, 2, [1, 3])  ← bas de la pile\n",
    "\n",
    "Cette pile contient les diverses hypothèses à explorer pour quelques cases. Dans cet exemple, on peut supposer que l'algorithme a testé la valeur 5 dans la case de coordonnées $(0,2)$, et il restera donc ultérieurement les deux autres hypothèses (les valeurs 1 et 3) à explorer ; par la suite une seconde case a été empilée et deux hypothèses pour cette case doivent être explorées (les valeurs 3 et 9).\n",
    "\n",
    "Le backtracing consiste ici à dépiler les cases du haut de la pile pour lesquelles plus aucune hypothèse ne peut être explorée.\n",
    "\n",
    "Écrire une fonction `backtracking` prenant en arguments une grille et une pile de triplets ($x$, $y$, $hypothèses$). La pile est implémentée à l'aide d'une liste et les derniers éléments empilés sont à la fin. La fonction remonte de la droite vers la gauche en dépilant les triplets jusqu'à en rencontrer un pour lequel il reste des hypothèses inexplorées (le troisième élément du triplet doit donc être une liste non vide). Chaque fois qu'une case est dépilée, on remet la valeur 0 dans la grille à l'emplacement correspondant (on efface la valeur provisoirement testée). Il n'est pas nécessaire de vérifier si la pile est vide ou non car les conditions du problème garantissent que l'on rencontrera toujours au moins une case adéquate. La fonction ne retourne rien."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def backtracking(grid, stack):\n",
    "    while len(stack[-1][2]) == 0:\n",
    "        x, y, _ = stack.pop() # on dépile\n",
    "        grid[y][x] = 0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Choix d'une hypothèse pour une case donnée\n",
    "\n",
    "Une liste d'hypothèses étant disponible pour une case donnée, on choisira n'importe laquelle ; il est d'usage en Python de travailler sur les fins de listes plutôt que sur les débuts car la complexité de ces opérations est alors en $O(1)$ ; on retiendra donc systématiquement _la dernière hypothèse_ de la liste.\n",
    "\n",
    "Écrire une fonction `decision` prenant en arguments une grille et une pile de triplets ($x$, $y$, $hypothèses$) semblable à celle de la question précédente. La fonction dépile le dernier triplet, prélève l'une des hypothèses de la liste, met la valeur correspondante dans la grille à l'emplacement correspondant, et empile à nouveau le triplet (contenant désormais une hypothèse de moins) sur la pile. La fonction ne retourne rien."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def decision(grid, stack):\n",
    "    x, y, c = stack.pop()\n",
    "    grid[y][x] = c.pop()\n",
    "    stack.append( (x, y, c) ) # on rempile la case"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Résolution finale du problème\n",
    "\n",
    "Écrire une fonction `solve` prenant en argument une grille. La fonction initialise une pile vide, compte le nombre initial de cases vides, puis répète les opérations suivantes tant que la taille de la pile est inférieure au nombre initial de cases vides :\n",
    "\n",
    "  * les données ($x$, $y$, $hypothèses$) correspondant à une case « intéressante » (qui minimise le nombre d'hypothèses) sont calculées ;\n",
    "  * ces données sont empilées ;\n",
    "  * la procédure de _backtracking_ est effectuée (au cas où la dernière case empilée serait en situation de blocage) ;\n",
    "  * une décision est prise (à l'aide de la fonction `decision`) pour la case située en haut de la pile.\n",
    "\n",
    "La fonction `solve` travaille _en place_ ; elle ne retourne rien mais modifie progressivement la grille de départ en la remplissant."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def solve(grid):\n",
    "    stack = []\n",
    "    # on compte le nombre de cases vides\n",
    "    nzeros = 0\n",
    "    for kx in range(9):\n",
    "        for ky in range(9):\n",
    "            if grid[ky][kx] == 0: nzeros += 1\n",
    "    while len(stack) < nzeros:\n",
    "        # on recherche et on empile la case la plus contrainte\n",
    "        stack.append( min_cell(grid) )\n",
    "        # on remonte en dépilant toutes les cases sans candidat valide\n",
    "        backtracking(grid, stack)\n",
    "        # l'opération précédente garantit que la case en haut de la pile a\n",
    "        # au moins un candidat qui va être retenu\n",
    "        decision(grid, stack)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Test de l'algorithme\n",
    "\n",
    "On peut désormais tester la fonction `solve` à l'aide des lignes suivantes :"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "solve(G)\n",
    "for row in G:\n",
    "    print(row)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
